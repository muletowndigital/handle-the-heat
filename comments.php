<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.  The actual display of comments is
 * handled by a callback to twentyten_comment which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php if (post_password_required()) : ?>
    <p class="nopassword"><?php _e('This post is password protected. Enter the password to view any comments.', 'twentyten'); ?></p>
    </div><!-- #comments -->
    <?php
    /* Stop the rest of comments.php from being processed,
     * but don't kill the script entirely -- we still have
     * to fully load the template.
     */
    return;
endif;
?>

<?php
// You can start editing here -- including this comment!
?>



<?php if (comments_open()) : ?>
    <div id="respond" class="comment-respond">
    <h4 id="respondheading" class="comment-reply-title"><?php comment_form_title('Leave a Comment & Rating'); ?></h4>
    <h4 id="replyheading"
        class="comment-reply-title reply-title"><?php comment_form_title('Add a Review or Question'); ?></h4>

    <div class="cancel-comment-reply">
        <p><small><?php cancel_comment_reply_link("Cancel Reply"); ?></small></p>
    </div>

    <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
        <p>You must be <a href="<?php echo wp_login_url(get_permalink()); ?>">logged in</a> to post a comment.</p>
    <?php else : ?>

        <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform"
              class="comment-form">

            <p class="comment-form-comment"><label for="comment"><small>Comment</small></label><textarea name="comment"
                                                                                                         id="comment"
                                                                                                         class="commentinput"
                                                                                                         cols="70"
                                                                                                         rows="10"
                                                                                                         tabindex="4"></textarea>
            </p>

            <p class="comment-form-options">
            <span><input id="comment-option-question" name="comment-option-question" type="checkbox"/><label
                        for="comment-option-question">I have a question</label></span>
                <span><input id="comment-option-review" name="comment-option-review" type="checkbox"/><label
                            for="comment-option-review">I made this</label></span>
            </p>

            <?php if (is_user_logged_in()) : ?>

                <p class="logged-in-state">Logged in as <a
                            href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.
                    <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out
                        &raquo;</a></p>

            <?php else : ?>

                <p><label for="author"><small>Name<?php if ($req) echo "*"; ?></small></label><input type="text"
                                                                                                     name="author"
                                                                                                     id="author"
                                                                                                     class="commentinput"
                                                                                                     value="<?php echo esc_attr($comment_author); ?>"
                                                                                                     size="22"
                                                                                                     tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
                </p>

                <p><label for="email"><small>Email<?php if ($req) echo "*"; ?></small></label><input type="text"
                                                                                                     name="email"
                                                                                                     id="email"
                                                                                                     class="commentinput"
                                                                                                     value="<?php echo esc_attr($comment_author_email); ?>"
                                                                                                     size="22"
                                                                                                     tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
                </p>

                <p><label for="url"><small>Website URL</small></label><input type="text" name="url" id="url"
                                                                             class="commentinput"
                                                                             value="<?php echo esc_attr($comment_author_url); ?>"
                                                                             size="22" tabindex="3"/>
                </p>

            <?php endif; ?>

            <?php global $wp_subscribe_reloaded;
            if (isset($wp_subscribe_reloaded)) {
                echo $wp_subscribe_reloaded->stcr->subscribe_reloaded_show();
            } ?>

            <div class="must-rate-error">
                <p>*Please select a rating to complete your comment.</p>
            </div>

            <?php do_action('comment_form_after'); ?>
            <?php do_action('comment_form_after_fields'); ?>
            <p><input name="submit" type="submit" id="submit" tabindex="5" value="Post Comment"/>
                <?php comment_id_fields(); ?>
            </p>
            <?php do_action('comment_form', $post->ID); ?>

        </form>
        </div>

    <?php endif; // If registration required and not logged in ?>


    <?php if (!empty($comments_by_type['comment'])) : ?>

        <!--    --><?php //if (get_comment_pages_count() > 1 && get_option('page_comments')) : // Are there comments to navigate through? ?>
        <!--        <div class="commentnav">-->
        <!--            --><?php //paginate_comments_links(); ?>
        <!--            <div class="alignright"><a href="#respondheading">Leave A Comment &raquo;</a></div>-->
        <!--        </div>-->
        <!--    --><?php //endif; // check for comment navigation ?>


               <?php
                $comments = get_comments(array(
                    'post_id' => $post->ID,
               ));
                $count_comment_option_question = 0;
                foreach($comments as $comment){
                    
                     $comment_ID = $comment->comment_ID;
                  // print_r($comment_ID);echo "<br>";
                   if (get_comment_meta($comment_ID, 'comment-option-review', true)) {
                        $count_comment_option_question++;
  
                       
                  }
                }           
                
                 ?>

        <div id="commentfilter" class="comment-filter">
            <a href="" id="comment-filter-comment" class="active">All Comments (<?php echo get_comments_number() ;?>)</a>
            <a href="" id="comment-filter-question">Questions</a>
            <a href="" id="comment-filter-review">I Made This (<?php echo $count_comment_option_question; ?>)</a>
        </div>
        <ol id="commentlist">
            <?php
            /* Loop through and list the comments. Tell wp_list_comments()
             * to use twentyten_comment() to format the comments.
             * If you want to overload this in a child theme then you can
             * define twentyten_comment() and that will be used instead.
             * See twentyten_comment() in twentyten/functions.php for more.
             */
            wp_list_comments(array('callback' => 'custom_comment', 'type' => 'comment'));
            ?>
        </ol>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // Are there comments to navigate through? ?>
            <div class="commentnav">
                <div class="commentnav__inner">
                    <?php paginate_comments_links(); ?>

                </div>
            </div> <!-- .commentnav -->
        <?php endif; // check for comment navigation ?>
    <?php else : // this is displayed if there are no comments so far ?>

        <?php if (comments_open()) : ?>
            <!-- If comments are open, but there are no comments. -->

        <?php else : // comments are closed ?>
            <!-- If comments are closed. -->
            <p class="nocomments">Comments are closed.</p>

        <?php endif; ?>
    <?php endif; ?>

    <?php if (!empty($comments_by_type['pings'])) : ?>
        <h3 id="pings">Trackbacks/Pingbacks</h3>
        <ol id="pinglist">
            <?php wp_list_comments(array('callback' => 'custom_comment', 'type' => 'pings')); ?>
        </ol>
    <?php endif; ?>

<?php endif; // if you delete this the sky will fall on your head ?>
