
<div class="contentabout">

	<div class="left-contentabout"><img alt="Tessa Arias" src='<?php echo get_template_directory_uri(); ?>/images/people/tessa-gravatar.jpeg' width="165" height="165"></div>

	<div class="right-contentabout">

		<div class="about">

			<h3>About <?php the_author_meta( 'first_name' ); ?>...</h3>

			<?php
				$facebook_user  = get_the_author_meta('facebook');
				$pinterest_user  = get_the_author_meta('pinterest');
				$youtube_user  = get_the_author_meta('youtube');
				$instagram_user  = get_the_author_meta('instagram');
				$twitter_user  = get_the_author_meta('twitter');
			?>

			<p><?php the_author_meta( 'description' ); ?></p>

			<div>

				<strong>Find <?php the_author_meta( 'first_name' ); ?> on &nbsp; </strong>

				<?php if( ! empty( $facebook_user ) ) { ?>

					<a href="<?php echo the_author_meta( 'facebook' ); ?>"><i class="fa fa-facebook circle"></i></a>

				<?php } ?>

				<?php if( ! empty( $pinterest_user ) ) { ?>

					<a href="<?php echo the_author_meta( 'pinterest' ); ?>"><i class="fa fa-pinterest circle"></i></a>

				<?php } ?>

				<?php if( ! empty( $youtube_user ) ) { ?>

					<a href="<?php echo the_author_meta( 'youtube' ); ?>"><i class="fa fa-youtube-play circle"></i></a>

				<?php } ?>

				<?php if( ! empty( $instagram_user ) ) { ?>

					<a href="<?php echo the_author_meta( 'instagram' ); ?>"><i class="fa fa-instagram circle"></i></a>

				<?php } ?>

				<?php if( ! empty( $twitter_user ) ) { ?>

					<a href="https://twitter.com/<?php echo the_author_meta( 'twitter' ); ?>"><i class="fa fa-twitter circle"></i></a>

				<?php } ?>

			</div>

		</div>

	</div> <!-- rundown-right -->

</div> <!-- main-rundown -->