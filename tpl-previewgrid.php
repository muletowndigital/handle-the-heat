<?php 
	// displays the recipe rundown, yield, prep, ingredients, directions and related recipes
?>


	<?php if ( (get_field('recipe_yield')) || (get_field('recipe_prep')) || (get_field('recipe_time')) || (get_field('recipe_ingredients')) || (get_field('recipe_directions')) ) { ?>

		<?php
			$text = get_the_title();
			$filtered_words = array('How to Make', 'How to make',);
			$zap = '';
			$filtered_text = str_replace($filtered_words, $zap, $text);
		?>
		<h2 class="how"><span class="how">How to make</span> <span class="how_title"><?php echo $filtered_text; ?></span></h2>

		<div class="title">
			<div class="share">
				<?php get_template_part('tpl-single-share'); ?>
			</div>
		</div>

		<?php get_template_part('tpl-single-prep'); ?>

		<?php the_field('recipe_meta'); ?>

		<?php if ( get_field('recipe_ingredients') ) { ?>
			<div class="ingredients">
				<h3 class="title">Ingredients</h3>
				<span class="ingredient"><?php the_field('recipe_ingredients'); ?></span>
			</div>
		<?php } ?>

		<?php if ( get_field('recipe_directions') ) { ?>
			<div class="directions">
				<h3 class="title">Directions</h3>
				<span class="instructions"><?php the_field('recipe_directions'); ?></span>
			</div>
		<?php } ?>

		<?php if ( get_field('recipe_source') ) { ?>
			<p><?php the_field('recipe_source'); ?></p>
		<?php } ?>

	<?php } ?>

