<?php // yield, cook & prep time
global $post;
$recipe = get_query_var( 'recipe' ); ?>

<?php if ( get_field( 'non_recipe' ) != 'Yes' ) {

	if ( $recipe || get_field( 'recipe_yield' ) || get_field( 'recipe_prep' ) || get_field( 'recipe_time' ) ) { ?>

		<div class="prep">

			<?php if ( ( $recipe && $recipe->servings() ) || get_field( 'recipe_yield' ) ) { ?>

				<p><i class="fa fa-cutlery"></i> <strong>Yield:</strong> <span class="yield">

					<?php if ( $recipe && $recipe->servings() ) {

						echo $recipe->servings() . ' ' . $recipe->servings_unit();

					}

					else {

						the_field( 'recipe_yield' );

					} ?>

				</span></p>

			<?php }

			if ( ( $recipe && $recipe->prep_time() ) || ( ! $recipe && get_field('recipe_prep') ) ) { ?>

				<p class="half">

					<i class="fa fa-clock-o"></i>

					<strong>Prep Time:</strong>

					<?php if ( $recipe && $recipe->prep_time() ) { ?>

						<span class="preptime">

							<?php echo $recipe->prep_time_formatted(); ?>

						</span>

					<?php }

					else if ( ! $recipe && get_field('recipe_time') ) {

						if ( get_field( 'recipe_prep_time_group' ) == '15' ) { ?>

							<span class="preptime"><span class="value-title" title="PT15M"></span><?php the_field('recipe_prep'); ?></span>

						<?php }

						else if ( get_field( 'recipe_prep_time_group' ) == '30' ) { ?>

							<span class="preptime"><span class="value-title" title="PT30M"></span><?php the_field('recipe_prep'); ?></span>

						<?php }

						else if ( get_field( 'recipe_prep_time_group' ) == '45' ) { ?>

							<span class="preptime"><span class="value-title" title="PT45M"></span><?php the_field('recipe_prep'); ?></span>

						<?php }

						else if ( get_field( 'recipe_prep_time_group' ) == '60' ) { ?>

							<span class="preptime"><span class="value-title" title="PT1H00M"></span><?php the_field('recipe_prep'); ?></span>

						<?php }

						else if ( get_field( 'recipe_prep_time_group' ) == '99' ) { ?>

							<span class="preptime"><span class="value-title" title="PT1H00M"></span><?php the_field('recipe_prep'); ?></span>

						<?php }

						else { ?>

							<?php the_field( 'recipe_prep' ); ?>

						<?php }

					} ?>

				</p>

			<?php } ?>

			<?php if ( ( $recipe && $recipe->cook_time() ) || ( ! $recipe && get_field('recipe_time') ) ) { ?>

				<p class="half">

					<i class="fa fa-clock-o"></i>

					<strong>Cook:</strong>

					<?php if ( $recipe && $recipe->cook_time() ) { ?>

						<span class="duration">

							<?php echo $recipe->cook_time_formatted(); ?>

						</span>

					<?php }

					else if ( ! $recipe && get_field('recipe_time') ) {

						if ( get_field( 'recipe_time_group' ) == '15' ) { ?>

							<span class="duration"><span class="value-title" title="PT15M"></span><?php the_field('recipe_time'); ?></span>

						<?php }

						else if ( get_field( 'recipe_time_group' ) == '30' ) { ?>

							<span class="duration"><span class="value-title" title="PT30M"></span><?php the_field('recipe_time'); ?></span>

						<?php }

						else if ( get_field( 'recipe_time_group' ) == '45' ) { ?>

							<span class="duration"><span class="value-title" title="PT45M"></span><?php the_field('recipe_time'); ?></span>

						<?php }

						else if ( get_field( 'recipe_time_group' ) == '60' ) { ?>

							<span class="duration"><span class="value-title" title="PT1H00M"></span><?php the_field('recipe_time'); ?></span>

						<?php }

						else if ( get_field( 'recipe_time_group' ) == '120' ) { ?>

							<span class="duration"><span class="value-title" title="PT2H00M"></span><?php the_field('recipe_time'); ?></span>

						<?php }

						else if ( get_field( 'recipe_time_group' ) == '180' ) { ?>

							<span class="duration"><span class="value-title" title="PT3H00M"></span><?php the_field('recipe_time'); ?></span>

						<?php }

						else if ( get_field( 'recipe_time_group' ) == '240' ) { ?>

							<span class="duration"><span class="value-title" title="PT4H00M"></span><?php the_field('recipe_time'); ?></span>

						<?php }

						else {

							the_field( 'recipe_time' );

						}

					} ?>

				</p>

			<?php } ?>

		</div>

	<?php }

} ?>