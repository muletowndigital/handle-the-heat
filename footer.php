<?php if (shortcode_exists('instagram-feed') && !is_single() && !is_front_page() && !is_page(37494)) : ?>
    <div id="instagram">
        <div class="container">
            <h3>Instagram</h3>
        </div>
        <?php echo do_shortcode('[instagram-feed]'); ?>
    </div>
<?php endif ?>
</div>
<div id="seen_on_list">
    <div class="container">
        <h6>As Seen On....</h6>
        <div class="feat-logos">
            <img src="<?php echo get_template_directory_uri(); ?>/images/feat-logos/npr.png" alt="NPR" width="108" height="36">
            <img src="<?php echo get_template_directory_uri(); ?>/images/feat-logos/people.png" alt="People" width="87" height="36">
            <img src="<?php echo get_template_directory_uri(); ?>/images/feat-logos/time.png" alt="Time" width="115" height="36">
            <img src="<?php echo get_template_directory_uri(); ?>/images/feat-logos/glamour2.png" alt="Glamour" width="152" height="36">
            <img src="<?php echo get_template_directory_uri(); ?>/images/feat-logos/readers-digest.png"
                 alt="Readers Digest" width="74" height="36">
            <img src="<?php echo get_template_directory_uri(); ?>/images/feat-logos/huffington-post.png"
                 alt="The Huffington Post" width="93" height="36">
            <img src="<?php echo get_template_directory_uri(); ?>/images/feat-logos/buzzfeed.png" alt="BuzzFeed" width="201" height="36">
        </div>
    </div>
</div>

<?php
$footer_logo = get_field('footer_logo', 'options');
$copyright = get_field('copyright', 'options');

?>

<footer id="footer" class="footer">
    <div class="container">

        <?php if ($footer_logo) { ?>
            <div class="footer__logo">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/hth-logo-footer-v2.svg" alt="Handle the Heat Logo" />
                </a>
            </div>
        <?php } ?>

        <div class="footer__main">
            <div class="footer__menus">

                <div class="footer__first-col">
                    <nav class="footer-menu footer-menu__first">
                        <p class="footer__heading"><?php _e('COMPANY', 'handletheheatstage'); ?></p>
                        <?php wp_nav_menu(array('theme_location' => 'footer-menu')); ?>
                    </nav>

                    <?php if (have_rows('socials', 'options')): ?>
                        <div class="footer__socials">
                            <?php while (have_rows('socials', 'options')) : the_row(); ?>

                                <?php $social_icon = get_sub_field('icon'); ?>
                                <?php $social_link = get_sub_field('url'); ?>
                                <a class="footer__social" target="_blank"
                                   href="<?php echo $social_link; ?>"><?php echo $social_icon; ?></a>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="footer__second-col">
                    <nav class="footer-menu footer-menu__second">
                        <p class="footer__heading"> <?php _e('MENU', 'handletheheatstage'); ?></p>
                        <?php wp_nav_menu(array('theme_location' => 'footer-menu-one')); ?>
                    </nav>
                </div>

                <div class="footer__third-col">
                    <nav class="footer-menu footer-menu__third">
                        <p class="footer__heading"><?php _e('ACCOUNT', 'handletheheatstage'); ?></p>
                        <?php wp_nav_menu(array('theme_location' => 'footer-menu-two')); ?>
                    </nav>
                </div>

            </div>

            <div class="footer__copy">
                <nav class="footer-menu footer-menu__forth">
                    <?php wp_nav_menu(array('theme_location' => 'footer-menu-three')); ?>
                </nav>
                <div class="footer__copyright">
                    <?php echo $copyright; ?>
                </div>
            </div>

        </div>

    </div>
</footer>

<!--  for mobile -->

<div class="headermobile mm">

    <div class="mobile-nav">
        <nav class="menu" id="primary-menu-mobile">

            <div class="side-search header-search mm__search">
                <?php echo do_shortcode('[wd_asp id=4]'); ?>
            </div>

            <?php wp_nav_menu(array(
                'container' => false,
                'theme_location' => 'mobile-menu',
                'menu' => "Mobile Theme Menu",
            )); ?>
        </nav>

        <?php if (have_rows('socials', 'options')): ?>
            <div class="mm__socials">
                <?php while (have_rows('socials', 'options')) : the_row(); ?>

                    <?php $social_icon = get_sub_field('icon'); ?>
                    <?php $social_link = get_sub_field('url'); ?>
                    <a class="mm__social" target="_blank"
                       href="<?php echo $social_link; ?>"><?php echo $social_icon; ?></a>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>

    </div>
</div> <!-- headermobile -->

<?php wp_footer(); ?>
</body>
</html>
