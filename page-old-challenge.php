<?php
    /* Template Name: Adam Test */
?>
<?php get_header(); ?>
<h1>Silverman</h1>
<div class="content">
    <div class="container">
        <h1 class="mcr-title"><?php the_title(); ?></h1>
        <div class="mcr-winners"> <!-- mcrwinners-->
            <div class="recipetitle"><div class="text"><?php the_field('mcr_monthly_challenge_recipe_name'); ?></div></div>
            <div class="mcr-winner-section">
                <div class="mcr-winners-content"><?php the_field('mcr_winners_content'); ?></div>
                <h4><?php the_field('mcr_winners_title'); ?><br><hr></h4>
                <div class="mcr-winners-subtitle"><?php the_field('mcr_winners_sub_title'); ?></div>
                    <div class="mcr-winners-prizes">
                    <?php if( have_rows('mcr_winners_prizes') ): 
                          while( have_rows('mcr_winners_prizes') ): the_row(); 
                          // vars
                          $mcr_winner1_recipe_image = get_sub_field('mcr_winner1_recipe_image');
                          $mcr_winner1_prize_image = get_sub_field('mcr_winner1_prize_image');
                          $mcr_winner1_name_content = get_sub_field('mcr_winner1_name_content');
                          $mcr_winner2_recipe_image = get_sub_field('mcr_winner2_recipe_image');
                          $mcr_winner2_prize_image = get_sub_field('mcr_winner2_prize_image');
                          $mcr_winner2_name_content = get_sub_field('mcr_winner2_name_content');
                          $mcr_winner3_recipe_image = get_sub_field('mcr_winner3_recipe_image');
                          $mcr_winner3_prize_image = get_sub_field('mcr_winner3_prize_image');
                          $mcr_winner3_name_content = get_sub_field('mcr_winner3_name_content');
                          
                          if(!empty($mcr_winner1_name_content)){
                            echo '<div class="mcr-winners-prize1">
                                    <div><img src="'.$mcr_winner1_recipe_image.'"/></div>
                                    <div><img src="'.$mcr_winner1_prize_image.'"/></div>
                                    <div>'.$mcr_winner1_name_content.'</div>
                                  </div>';  
                          }
                          if(!empty($mcr_winner2_name_content)){
                            echo '<div class="mcr-winners-prize2">
                                    <div><img src="'.$mcr_winner2_recipe_image.'"/></div>
                                    <div><img src="'.$mcr_winner2_prize_image.'"/></div>
                                    <div>'.$mcr_winner2_name_content.'</div>
                                  </div>';  
                          }
                          if(!empty($mcr_winner3_name_content)){
                            echo '<div class="mcr-winners-prize3">
                                    <div><img src="'.$mcr_winner3_recipe_image.'"/></div>
                                    <div><img src="'.$mcr_winner3_prize_image.'"/></div>
                                    <div>'.$mcr_winner3_name_content.'</div>
                                  </div>';  
                          }
                    ?>
                    
                    <?php endwhile; ?>
                    <?php endif; ?>
                    </div>
            </div>
        </div> <!-- mcrwinners-->
        <div class="mcr-section-buttons">
            <ul>
            <?php if(get_field('mcr_sponsor_title')) { 
                echo "<li><a href='#sponsor-info'>Sponsor Info</a></li>";
            } ?>
            <?php if(get_field('mcr_recipe_link')) { 
                echo "<li><a href='#the-recipe'>The Recipe</a></li>";
            } ?>
            <?php if(get_field('mcr_participant_entries')) { 
                echo "<li><a href='#participant-entries'>Participant Entries</a></li>";
            } ?>
            
            <?php if(get_field('mcr_fun_facts')) { 
                echo "<li><a href='#fun-facts'>Fun Facts</a></li>";
            } ?>
            <?php if(get_field('mcr_about_monthly_challenge_content')) { 
                echo "<li><a href='#challenge-info'>Challenge Info</a></li>";
            } ?>
            </ul>
        </div>
        <?php if(get_field('mcr_sponsor_title')) { ?>
        <div class="mcr-sponsor" id="sponsor-info"> <!-- mcrsponsor-->
            <h4><?php the_field('mcr_sponsor_title'); ?> <br><hr></h4>
            <div class="mcr-sponsor-img"><img src="<?php echo get_field('mcr_sponsor_image'); ?>"/></div>
            <div class="mcr-sponsor-content"><?php the_field('mcr_sponsor_content'); ?></div>
            <div class="mcr-sponsor-content-buttons">
                <?php if( have_rows('mcr_sponsor_content_buttons') ): 
                      while( have_rows('mcr_sponsor_content_buttons') ): the_row(); 
                      // vars
                      $mcr_sponsor_content_button1 = get_sub_field('mcr_sponsor_content_button1');
                      $mcr_sponsor_content_button2 = get_sub_field('mcr_sponsor_content_button2');
                      $mcr_sponsor_content_button3 = get_sub_field('mcr_sponsor_content_button3');
                      $mcr_sponsor_content_button1_link = get_sub_field('mcr_sponsor_content_button1_link');
                      $mcr_sponsor_content_button2_link = get_sub_field('mcr_sponsor_content_button2_link');
                      $mcr_sponsor_content_button3_link = get_sub_field('mcr_sponsor_content_button3_link');
                      
                      if(!empty($mcr_sponsor_content_button1)){
                        echo '<div class="mcr-sponsor-content-button"><a href="'.$mcr_sponsor_content_button1_link.'"/>'.$mcr_sponsor_content_button1.'</a></div>';  
                      }
                      if(!empty($mcr_sponsor_content_button2)){
                        echo '<div class="mcr-sponsor-content-button"><a href="'.$mcr_sponsor_content_button2_link.'"/>'.$mcr_sponsor_content_button2.'</a></div>';  
                      }
                      if(!empty($mcr_sponsor_content_button3)){
                        echo '<div class="mcr-sponsor-content-button"><a href="'.$mcr_sponsor_content_button3_link.'"/>'.$mcr_sponsor_content_button3.'</a></div>';  
                      }
                ?>
                
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div> <!-- mcrsponsor-->
        <?php } ?>
        
        <div class="mcr-recipelink" id="the-recipe"> <!-- mcrrecipelink-->
            <a href="<?php the_field('mcr_recipe_link'); ?>">Click to go to Recipe</a>
        </div> <!-- mcrrecipelink-->
         
        <div class="mcrentries" id="participant-entries">
            <h4><?php the_field('mcr_participant_entries'); ?></h4>
            <div><?php the_field('mcr_participant_entries_widget_code'); ?></div>
        </div>
        
        <div class="mcr-optin"> <!--mcroptin -->
            <div class="mcr-optin-inner">
				<div class="mcr-optin-text">
                        <div class="optin-img"><img src="<?php the_field('mcr_opt_in_image'); ?>"></div>
                        <div class="optin-text"><?php the_field('mcr_opt_in_text'); ?></div>
                        
				</div>
				<div class="mcr-optin-button">
				        <div class="optin-button"><?php the_field('mcr_opt_in_button_text'); ?></div>
				</div>
			</div>
		</div><!--mcroptin -->
        <div class="mcr-funfacts" id="fun-facts"> <!-- mcrfunfacts-->
            <div class="mcr-funfacts-inner"><img src="<?php echo get_field('mcr_fun_facts'); ?>"/></div>
        </div> <!-- mcrfunfacts-->
        <div class="mcr-abtchallenge" id="challenge-info"> <!-- mcraboutmonthlychallenge-->
            <div class="mcr-abtchallengetitle"><?php the_field('mcr_about_monthly_challenge_title'); ?></div>
            <div class="mcr-abtchallengecontent"><?php the_field('mcr_about_monthly_challenge_content'); ?></div>
            <div class="mcr-abtchallengebutton"><?php the_field('mcr_about_monthly_challenge_button'); ?></div>
            
            <div class="mcr-abtchallenge-section2">
                <div class="mcr-abtchallenge-section2-title"><?php the_field('mcr_about_monthly_challenge_section2_title'); ?></div>
                <div class="mcr-abtchallenge-section2-img"><img src="<?php echo get_field('mcr_about_monthly_challenge_section2_image'); ?>"/></div>
                <div class="mcr-abtchallenge-section2-content">
                    <div><?php the_field('mcr_about_monthly_challenge_section2_content'); ?></div>
                    <script type="text/javascript">
                    function handleChange() {
                        var arr = document.getElementById( 'challenge' ) ;
                        window.location.href = arr.value ;
                        //window.location(arr.value, "_blank");
                    } 
                    </script>
                    <div class="mcr-pastchallenge"><!-- mcr-pastchallenge -->
                    <div><strong>View our past challenges:</strong></div>
                    <div class="jumpto"> Jump to a challenge &nbsp;
                            <?php    
                            $args = array('cat'=>'537,2093');
                            $query = new WP_Query( $args );
                            echo '<select name="challenge" id="challenge" onchange="handleChange()">';
                            echo '<option> Please Select</option>';
                            // The Loop
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    echo '<option value="'.get_the_permalink().'">' . get_the_title() . '</option>';
                                    
                                        
                                }
                            } 
                            echo '</select>';
                            /* Restore original Post Data */
                            wp_reset_postdata();
                            ?>
                    </div> <!-- jumpto -->
                    </div> <!-- mcr-pastchallenge -->
                </div>
                
                
            </div>
            
            <div class="mc-bakingchallenge-feed"><?php the_field('mcr_social_feed_gallery'); ?></div>
            
        </div><!-- mcraboutmonthlychallenge-->
        <div class="mcr-nextoptin"> <!-- mcrnextoptin-->
            <div class="mcr-nextoptin-inner">
				<div class="mcr-nextoptin-text">
                    <div class="nextoptin-text"><?php the_field('mcr_signup_next_challenge_text'); ?></div>
                </div>
				<div class="mcr-nextoptin-button">
				        <div class="nextoptin-button"><?php the_field('mcr_signup_next_challenge_button_text'); ?></div>
				</div>
			</div>   
        </div> <!-- mcrnextoptin-->
        <div class="mcr-about" id="mcr-about-tessa"> <!-- mcr-about -->
            <h4><?php the_field('mcr_about_tessa_titlte'); ?> <br><hr></h4>
		    <div class="mcr-about-img"><img src="<?php echo get_field('mcr_about_tessa_image'); ?>"/></div>
            <div class="mcr-about-right">
                <div class="mcr-about-rightcontent">
                    <div class="mcr-about-rightcontent-inner"><?php the_field('mcr_about_tessa_content'); ?></div>
                </div>
            </div>
        </div> <!-- mcr-about -->
        
    </div>
</div>
<?php get_footer(); ?>