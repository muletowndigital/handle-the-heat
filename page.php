<?php get_header(); ?>


	<div class="content">
		
		<div class="container">
					
			<div class="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php the_content(); ?>

				<?php endwhile; endif; ?>


			</div>

			<div class="sidebar launch">
				<?php get_sidebar('launch'); ?>
			</div>

		</div>

	</div>


<?php get_footer(); ?>