
<?php 
    // Template Name: Update Your Details
?>

<?php get_header(); ?>


    <div class="content">
        
        <div class="container">
                    
            <div class="main">
                    
                <h4>Need to update your details?</h4>
                
                <?php if ( is_user_logged_in() ) { ?>
                    
                    <span style="display:none;" class="pink">Username:</span> <?php // echo $current_user->user_login; ?>

                    <?php the_content(); ?>

                <?php } else { ?>
                    
                    <p>Your are not logged in. Please <a href="/login">login</a> or <a href="/register">register</a> to get started.</p>

                <?php } ?>

            </div>

            <div class="sidebar launch">
                <?php get_sidebar('launch'); ?>
            </div>

		</div>

	</div>


<?php get_footer(); ?>