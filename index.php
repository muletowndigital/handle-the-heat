<?php get_header(); ?>

	<div class="content">
		
		<div class="container">
					
			<div class="main">

				<h2 class="title">Blog</h2>

				<?php // echo do_shortcode('[searchandfilter id="9571"]'); ?>

				<?php query_posts( array( 'post_type' => array( 'blog', 'post'), 'paged' => get_query_var( 'paged' ) ) ); ?>

				<?php get_template_part('tpl-blog'); ?>			

			</div>

			<div class="sidebar launch">
				<?php get_sidebar('launch'); ?>
			</div>

		</div>

	</div>

<?php get_footer(); ?>