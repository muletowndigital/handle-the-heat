<!doctype html>

<html>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <!--[if IE ]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="Copyright"
          content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png">
    <meta name="facebook-domain-verification" content="orlzoqpp60je92dykm04cgat14paj4" />

    <!-- SegMetrics -->
    <script type="text/javascript">
        var _segq = _segq || [];
        var _segs = _segs || {};
        (function () {
            var dc = document.createElement('script');
            dc.type =
                'text/javascript';
            dc.async = true;
            dc.src =
                '//tag.segmetrics.io/aM6PbN.js';
            var s =
                document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(dc, s);
        })();
    </script>
    <!-- SegMetrics END -->

    <!-- Hotjar Tracking Code for www.handletheheat.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:54253,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Install this snippet AFTER the Hotjar tracking code. -->
    <script>
        var userId = your_user_id || null; // Replace your_user_id with your own if available.
        window.hj('identify', userId, {
            // Add your own custom attributes here. Some EXAMPLES:
            // 'Signed up': '2019—06-20Z', // Signup date in ISO-8601 format.
            // 'Last purchase category': 'Electronics', // Send strings with quotes around them.
            // 'Total purchases': 15, // Send numbers without quotes.
            // 'Last purchase date': '2019-06-20Z', // Send dates in ISO-8601 format.
            // 'Last refund date': null, // Send null when no value exists for a user.
        });
    </script>

    <!-- Pinterest Tag -->
    <script>
        !function(e){if(!window.pintrk){window.pintrk = function () {
            window.pintrk.queue.push(Array.prototype.slice.call(arguments))};var
            n=window.pintrk;n.queue=[],n.version="3.0";var
            t=document.createElement("script");t.async=!0,t.src=e;var
            r=document.getElementsByTagName("script")[0];
            r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js");
        pintrk('load', '2613699269371', {em: '<user_email_address>'});
        pintrk('page');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none;" alt=""
             src="https://ct.pinterest.com/v3/?event=init&tid=2613699269371&pd[em]=<hashed_email_address>&noscript=1"
        />
    </noscript>
    <!-- end Pinterest Tag -->

    <?php wp_head(); ?>
</head>
<?php
$no_stars_class = '';
if (get_field('remove_stars_from_page')) {
    $no_stars_class = 'no-stars-page';
}
?>
<body <?php body_class($no_stars_class); ?>>
<header id="header">
    <div class="container">

        <div class="logo">
            <a href="<?php echo get_site_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/images/hth-logo.svg" alt="Handle The Heat" width="255" height="62">
            </a>
        </div>

        <div class="top_wrap">

            <nav class="menu" id="primary-menu">
                <?php wp_nav_menu(array(
                    'container' => false,
                    //'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'theme_location' => 'main',
                ));
                ?>
            </nav>

            <div class="recipes-menu">
                <?php echo do_shortcode('[elementor-template id="50439"]') ;?>
            </div>

            <div class="topbar">
                <div class="side-search header-search">
                    <?php echo do_shortcode('[wd_asp id=4]'); ?>
                </div>
            </div>

        </div>

    </div>

    <div class="scroll-search"><button id="header-search-btn"><i class="fa fa-search"></i></button>
        <div class="side-search scroll-search__input">
            <?php echo do_shortcode('[wd_asp id=4]'); ?>
        </div>
    </div>

    <?php
    $output = do_shortcode('[elementor-template id="53323"]');
    if($output !== '') {
        echo '<div class="bs-banner">';
        echo $output;
        echo '</div>';
    }
    ?>

</header>

<!-- Mobile Triggers -->
<div class="scroll-icons">
    <div class="mobile-search"><i id="search-btn" class="fa fa-search"></i>
        <div class="side-search header-search mobile__search">
            <?php echo do_shortcode('[wd_asp id=4]'); ?>
        </div>
    </div>
    <div class="nav-triggers">
        <a class="hamburger js-mm-trigger" type="button" role="button" aria-label="Toggle Navigation">
            <span class="line1"></span>
            <span class="line2"></span>
            <span class="line3"></span>
        </a>
    </div>
</div>


<div class="newsletter-sticky">
    <?php echo do_shortcode('[elementor-template id="50441"]'); ?>
</div>

