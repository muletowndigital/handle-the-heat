<?php 
    // Template Name: Challenge Elementor V2
?>

<?php get_header(); ?>

    <div class="challenge-v2" id="challenge-v2">
        
        <div class="challenge-v2-container">
                    
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php the_content(); ?>

			<?php endwhile; endif; ?>

		</div>

	</div>

	<?php

		/*if( get_field('gleam') ){
	    	$gleam = get_field('gleam');
	    }

	    else {
			// gleam widget for results template
			$gleam = get_field('gleam', get_field('challenge'));
		}*/

    ?>

    <?php if(1<0) { ?>

    <div class="hth-challenges-gleam-competition">
        <?php if( strtotime(get_the_date('Y-m-d') . '+5 days') > strtotime('now') && get_field('gleam') ) { ?>
            <script type="text/javascript" src="https://widget.gleamjs.io/e.js" async="true"></script>
            <a class="e-widget no-button generic-loader" href="https://gleam.io/<?php echo $gleam['gleam_competition_id'];?>/" rel="nofollow"></a>
            <script type="text/javascript">
                (function(d, t) {
                    if (window.location.hash != '#gleam' && ('' + document.cookie).match(/(^|;)\s*Gleam<?php echo $gleam['gleam_competition_id'];?>=X($|;)/)) {
                        return;
                    }

                    var g = d.createElement(t),
                        s = d.getElementsByTagName(t)[0];
                    g.src = "https://widget.gleamjs.io/<?php echo $gleam['gleam_competition_id'];?>/ol.js";
                    s.parentNode.insertBefore(g, s);
                }(document, "script"));
            </script>
        <?php } else { ?>
            <script src="https://cdn.galleryjs.io/webpack/gallery-v2.launcher.js" async></script>
            <div data-gallery-include="<?php echo $gleam['gleam_gallery_id']; ?>"></div>
        </div>
        <?php } ?>

    <?php } ?>

<?php get_footer(); ?>