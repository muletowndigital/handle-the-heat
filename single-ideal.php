<?php get_header(); ?>





	<div class="content hrecipe">

		

		<div class="container">



			<div class="key_info">

		

				<div class="title">

					<div class="share">

						<?php get_template_part('tpl-single-share'); ?>

						<a href="<?php the_permalink(); ?>"><i class="fa fa-refresh circle"></i></a>

					</div>

					<h1 class="fn"><?php the_title(); ?></h1>

				</div>



				<?php get_template_part('tpl-single-keyinfo'); ?>



			</div>



			<div class="tabs_wrap">



				<div class="main" id="ideal">



				    <ul>

				        <li><a href="#tab-1">About this recipe</a></li>

				        <?php if ( get_field('recipe_ingredients') ) { ?>

				        	<li><a href="#tab-2">Ingredients</a></li>

				        <?php } ?>

				        <?php if ( get_field('recipe_directions') ) { ?>

				        	<li><a href="#tab-3">Directions</a></li>

				        <?php } ?>

				        <li style="display:none;"><a href="#tab-4">Reviews</a></li>

				    </ul>



				    <div id="tab-1">



						<div class="recipe_intro">

							<?php the_content(); ?>

						</div>



						<?php if ( get_field('recipe_rundown') ) { ?>

							<div class="rundown">

								<h3>Recipe Rundown</h3>

				    			<?php the_field('recipe_rundown'); ?>

				    		</div>

				    	<?php } ?>



						<?php $posts = get_field('related_recipes_picker');

						if( $posts ): ?>

							<div class="related">

								<h4>You'll also love...</h4>

							    <ul class="related_list">

								    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>

								        <?php setup_postdata($post); ?>

								        <li>

								            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

								            <a rel="nofollow" class="link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

								        </li>

								    <?php endforeach; ?>

							    </ul>

							    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

							</div>

						<?php endif; ?>



						<div class="comments_wrap">

							<?php comments_template('', true); ?>

						</div>



				    </div>

					

					<?php if ( get_field('recipe_ingredients') ) { ?>

					    <div id="tab-2">

					    	<div class="ingredients">

					    		<div class="share"><?php get_template_part('tpl-single-share'); ?></div>

					    		<h3 class="title print_only">Ingredients</h3>

					    		<?php the_field('recipe_meta'); ?>

					    		<span class="ingredient"><?php the_field('recipe_ingredients'); ?></span>

					    	</div>

					    </div>

					<?php } ?>



					<?php if ( get_field('recipe_directions') ) { ?>

					    <div id="tab-3">

					    	<div class="directions">

					    		<div class="share"><?php get_template_part('tpl-single-share'); ?></div>

					    		<h3 class="title print_only">Directions</h3>

					    		<span class="instructions"><?php the_field('recipe_directions'); ?></span>

					    	</div>

					    </div>

					<?php } ?>



				    <div id="tab-4" style="display:none;">

				    	<div class="reviews_wrap">

				    		<h4>Leave a review...</h4>

							<?php $reviewid = get_the_ID(); ?>

							<?php //echo do_shortcode('[WPCR_SHOW POSTID=" ' .$reviewid. ' " NUM="25" SHOWFORM="1" HIDEREVIEWS="0" HIDERESPONSE="0" SNIPPET="" MORE="" HIDECUSTOM="0"]'); ?>

				   		</div>

				    </div>



				    <div class="print_only copyright">

				    	<p>&copy; Handle the Heat - handletheheat.com</p>

				    </div>



				</div>



				<div class="sidebar ideal">

					<?php get_sidebar('ideal'); ?>

				</div>



			</div>

					

		</div>



	</div>





<?php get_footer(); ?>