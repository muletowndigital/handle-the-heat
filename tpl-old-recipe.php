<?php
	// displays the recipe rundown, yield, prep, ingredients, directions and related recipes
?>

	<?php get_template_part('tpl-single-popup'); ?>

	<?php if ( get_field('non_recipe') != "Yes"   ) { ?>

	<?php if ( (get_field('recipe_yield')) || (get_field('recipe_prep')) || (get_field('recipe_time')) || (get_field('recipe_ingredients')) || (get_field('recipe_directions')) ) { ?>
		<?php
			$text = get_the_title();
			$filtered_words = array('How to Make', 'How to make',);
			$zap = '';
			$filtered_text = str_replace($filtered_words, $zap, $text);
		?>
<div class="whole-recipe">
		<h2 id="jump"><div class="how">How to make</div> <div class="how_title"><?php echo $filtered_text; ?></div></h2>

        <div class="share"><?php get_template_part('tpl-single-share'); ?></div>
        <div class="recipeby">Recipe By <?php the_author_meta( 'first_name' ); ?> <?php the_author_meta( 'last_name' ); ?>, <?php bloginfo( 'name' ); ?></div>
<?php //} ?>
	<?php get_template_part('tpl-single-prep-fullcontent'); ?>

	<?php the_field('recipe_meta'); ?>

<div class="featured"><?php the_post_thumbnail('recipe_single', array('class'=> "photo")); ?></div>

	<?php if ( get_field('recipe_ingredients') ) { ?>
    	<div class="ingredients">

    		<h3 class="title">Ingredients</h3>
    		<div class="ingredient">
<?php

ob_start();
the_field('recipe_ingredients');
$ingredients = ob_get_clean();
$ingredients = str_replace(array('<p>','</p>'),array('','<br>'),$ingredients );
$ingredients = preg_replace('/^(.*)<br.*\/?>/m', '<li itemprop="ingredients">$1</li>', $ingredients );
$ingredients = str_replace( '<li>', '<li itemprop="ingredients">', $ingredients );
echo $ingredients;

?>

</div>
    	</div>
	<?php } ?>

	<?php if ( get_field('recipe_directions') ) { ?>
    	<div class="directions">
    		<h3 class="title">Directions</h3>
    		<span class="instructions"><?php the_field('recipe_directions'); ?></span>
    	</div>
	<?php } ?>

	<?php if ( get_field('recipe_source') ) { ?>
		<p class="recipe_source"><?php the_field('recipe_source'); ?></p>
	<?php } ?>
</div> <!-- whole recipe -->
<?php } ?>
<?php } ?>