<?php // the blog page - filtering between blog posts and recipes ?>

	<div class="blog_results">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<?php if ( 'blog' == get_post_type() ) { ?>

				<article>
					<?php the_post_thumbnail('thumbnail'); ?>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<span class="fa-stack fa">
						<i class="fa fa-comment fa-stack-2x"></i>
						<i class="fa fa-stack-1x"><?php comments_popup_link('0', '1', '%'); ?></i>
					</span>
					<p class="postmetadata"><strong><?php the_time('l, F jS, Y') ?></strong> - <?php the_terms( $post->ID, 'categories', ' Category: ', ', ' ); ?></p>
					<?php the_excerpt(); ?>
				</article>

			<?php } else { ?>

				<article class="recipe">
					<?php the_post_thumbnail('thumbnail'); ?>
					<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

					<?php if ( class_exists( 'WPRM_Recipe_Manager' ) ) {
						$recipe_id = 0;
	
						// Get first recipe in post content.
						if ( ! $recipe_id ) {

							$parent_post = get_post();
							$recipes = WPRM_Recipe_Manager::get_recipe_ids_from_content( $parent_post->post_content );

							if ( isset( $recipes[0] ) ) {

								$recipe_id = $recipes[0];

								$recipe = WPRM_Recipe_Manager::get_recipe( $recipe_id );

							}

						}

					}

					if ( $recipe_id && $recipe->summary() ) {

						echo '<p>' . $recipe->summary() . '</p>';
						$recipe_id = '';

					}

					else if ( get_field( 'recipe_intro' ) ) {

						the_field( 'recipe_intro' );

					} ?>

				</article>

			<?php } ?>

		<?php endwhile; endif; ?>

		<div class="pagination">

			<?php global $wp_query;
				$big = 999999999; // need an unlikely integer
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) );
			?>

		</div>

	</div>

