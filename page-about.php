<?php 
    // Template Name: About Page
?>

<?php get_header(); ?>

    <div class="content">
        
        <div class="container">
                    
            <div class="main full">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php remove_filter( 'the_content', 'wpautop' ); the_content(); ?>

				<?php endwhile; endif; ?>

            </div>

		</div>

	</div>


<?php get_footer(); ?>