(function ($) {
    $(document).ready(function () {

        var subMenu = $('.recipes-parent-item .sub-menu');

        $('.recipes-menu').clone().appendTo(subMenu);

        // Mobile Menu Trigger

        $('.js-mm-trigger').on('click', function(e) {
            $(this).toggleClass('js-mm-close');
            $('.mm').toggleClass('mm--show');
            $('html').toggleClass('locked');
            e.preventDefault();
        });

        $('#search-btn').on('click', function(e) {
            $('.mobile__search').toggleClass('mobile__search--show');
            e.preventDefault();
        });


        $('#header-search-btn').on('click', function(e) {
            $('.scroll-search__input').toggleClass('mobile__search--show');
            e.preventDefault();
        });

        //Mobile Submenu Menu Buttons
        var menuParent = $('.mm .menu-item-has-children');
        var button = '<div class="submenu-toggle"></div>';
        menuParent.append(button);
        $(".submenu-toggle").click(function () {
            $this = $(this);
            $this.siblings('.sub-menu').slideToggle();
            $this.toggleClass('open');
        });


        // $("#primary-menu-mobile").menumaker({
        //     title: "&nbsp;",
        //     format: "multitoggle"
        // });

    });

    $(window).scroll(function () {
        var top_pos = $(window).scrollTop();
        //var window_width = $(window).width();

        if(top_pos > 50) {
            $('.scroll-icons').addClass('mm--scrolled');
            $('.scroll-search').addClass('search--scrolled');
        }

        else {
            $('.scroll-icons').removeClass('mm--scrolled');
            $('.scroll-search').removeClass('search--scrolled');
        }

        lastScrollTop = top_pos;

    });

    $(window).scroll();
})(jQuery);
