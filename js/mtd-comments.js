jQuery(document).ready(function ($) {

    $('</br>').insertBefore($( ".right-rundown .rundown strong" ));

    $(function () {
        var top = $('.cta-sidebar__fixed').offset().top - parseFloat($('.cta-sidebar__fixed').css('marginTop').replace(/auto/, 0));
        var footTop = $('.sticky-stopper').offset().top - parseFloat($('.sticky-stopper').css('marginTop').replace(/auto/, 0));

        var maxY = footTop - $('.cta-sidebar__fixed').outerHeight();

        $(window).scroll(function (evt) {
            var y = $(this).scrollTop();
            if (y > top) {

                //Quand scroll, ajoute une classe ".fixed" et supprime le Css existant
                if (y < maxY) {
                    $('.cta-sidebar__fixed').addClass('fixed').removeAttr('style');
                } else {

                    //Quand la sidebar arrive au footer, supprime la classe "fixed" précèdement ajouté
                    $('.cta-sidebar__fixed').removeClass('fixed').css({
                        position: 'absolute',
                        top: (maxY - top) + 'px'
                    });
                }
            } else {
                $('.cta-sidebar__fixed').removeClass('fixed');
            }
        });
    });

    //Recipe Table Of Contents
    $('.js-contents-btn').on('click', function () {
        $('.recipe-contents').find('.recipe-contents__contents').slideToggle();
    });
    $('.recipe-contents__contents a').on('click', function () {
        $('.recipe-contents').find('.recipe-contents__contents').slideToggle();
    });

    $(".comment-author-tessaaarias1 > div > div.commentmeta > strong").each(function () {
        $(this).after($("<strong>").text(" @ Handle the Heat"));
    });

    $(".comment-author-haleywehner > div > div.commentmeta > strong").each(function () {
        $(this).replaceWith("<strong>Haley @ Handle the Heat</strong>");
    });

    $(".comment-author-emilyfink > div > div.commentmeta > strong").each(function () {
        $(this).replaceWith("<strong>Emily @ Handle the Heat</strong>");
    });

    // Deal with Categories Page

    $('.cat-page-mobile-trigger').on('click', function (e) {
        $('#mobileMenu').slideToggle(600);
        e.preventDefault();
    });

    //Mobile Filtering Bar
    $('#filter-toggle').on('click', function (e) {
        $('#recipes-filters').toggleClass('show');
        $('html').toggleClass('lock');
        e.stopPropagation();
        e.preventDefault();
    });
    $(document).click(function (e) {
        let drop = $('.static-element');
        let wrapBody = $('html');
        if (!drop.is(e.target) && drop.has(e.target).length === 0) {
            drop.removeClass('show');
            wrapBody.removeClass('lock');
        }
    });

    var buttonX = '<button class="close-filters"><i class="fas fa-times close-filters"></i></button>';
    $('#recipes-filters').prepend(buttonX);
    $('.close-filters').click(function () {
        $('#recipes-filters').removeClass('show');
        $('html').removeClass('lock');
    });


    //Categories Toggle Button
    var menuParent = $('.searchandfilter h4');
    var button = '<i class="fas fa-chevron-down"></i>';
    menuParent.append(button);
    $('.searchandfilter h4').click(function () {
        var $this = $(this);
        if ($this.siblings().hasClass('dropdown-show')) {
            $this.siblings().removeClass('dropdown-show');
            $this.find('i').removeClass('active');
        } else {
            $this.siblings().addClass('dropdown-show');
            $this.find('i').addClass('active');
        }
    });

    if ($('#search-recipe-grid .mix').length < 1) {
        $('#search-recipe-grid').append('<div id="no-results"><p>There were no results found with your filters</p></div>');
        $('.searchandfilter .sf-field-reset').clone().appendTo('#no-results');
        $('.facetwp-load-more').hide();
    } else {
        $('.no-results').hide();
        $('.facetwp-load-more').show();
    }

    $('.results-count').append('<div class="filtering-results"><p></p></div>');


    var selected = [];
    $('.searchandfilter input:checked').each(function () {
        selected.push($(this).next('label').text());
    });

    var colorsString = selected.join(", ");
    $('.filtering-results p').html(colorsString);

    var filtersReset = $('.sf-field-reset');

    var scheckFilter = $('.searchandfilter li ul li')
    scheckFilter.each(function () {
        if ($(this).hasClass('sf-option-active')) {
            $(this).parent('ul').addClass('has-checked-option');
            filtersReset.show();
        }
    });


    filtersReset.click(function () {
        filtersReset.hide();
    });


    var $root = $('html, body');

    $('a[href^="#"]').click(function () {
        var href = $.attr(this, 'href');

        $root.animate({
            scrollTop: $(href).offset().top
        }, 500, function () {
            window.location.hash = href;
        });

        return false;
    });

    $(".submit-entry").click(() => {
        Gleam.openPopupCampaign();
    });

    $(".submit-entry-text").click(() => {
        Gleam.openPopupCampaign();
    });

});

(function ($) {
    "use strict";

    //Search and Filter after AJAX finished scripts go here:
    $(document).on("sf:ajaxfinish", ".searchandfilter", function () {

        //Categories Toggle Button
        var menuParent = $('.searchandfilter h4');
        var filtersReset = $('.sf-field-reset');
        var button = '<i class="fas fa-chevron-down"></i>';
        menuParent.append(button);
        $('.searchandfilter h4').click(function () {
            var $this = $(this);
            if ($this.siblings().hasClass('has-checked-option')) {
                $this.siblings().removeClass('has-checked-option');
                $this.find('i').removeClass('active');

            } else if ($this.siblings().hasClass('dropdown-show')) {
                $this.siblings().removeClass('dropdown-show');
                $this.find('i').removeClass('active');
            } else {
                $this.siblings().addClass('dropdown-show');
                $this.find('i').addClass('active');

            }
        });

        var scheckFilter = $('.searchandfilter li ul li')
        scheckFilter.each(function () {
            if ($(this).hasClass('sf-option-active')) {
                $(this).parent('ul').addClass('has-checked-option');
            }
        });

        filtersReset.show();
        filtersReset.click(function () {
            setTimeout(function () {
                filtersReset.hide();
            }, 2000);
        });

        if ($('#search-recipe-grid .mix').length < 1) {
            $('#search-recipe-grid').append('<div id="no-results"><p>There were no results found with your filters</p></div>');
            $('.searchandfilter .sf-field-reset').clone().appendTo('#no-results');
            $('.facetwp-load-more').hide();
        } else {
            $('.no-results').hide();
            $('.facetwp-load-more').show();
        }

        var selected = [];
        $('.searchandfilter input:checked').each(function () {
            selected.push($(this).next('label').text());
        });

        var colorsString = selected.join(", ");

        $('.filtering-results p').html(colorsString);

    });



}(jQuery));

