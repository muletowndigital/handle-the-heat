/*
 * Filter comments and other comment functions
 * Count the comments of each type
 */

jQuery(document).ready(function($) {

    // Comment dropdown
    $(".comment-count").click(function(e) {
        $(this).toggleClass("active");
    });

    // Move comment form rating and change label
    $(".comment-form-wprm-rating").prependTo("#commentform");
    $(".comment-form-wprm-rating > label").text("Rate (if you’ve made the recipe)");

    // Filter comments
    $("#comment-filter-comment").click(function (e) {
        e.preventDefault();
        $(".comment-filter a").removeClass("active");
        $(this).addClass("active");
        $("#commentlist .comment").show();
    });

    $("#comment-filter-question").click(function (e) {
        e.preventDefault();
        $(".comment-filter a").removeClass("active");
        $(this).addClass("active");
        $("#commentlist .comment").hide();
        $("#commentlist .option-question").show();
        $("#commentlist .option-question .comment").show();
    });

    $("#comment-filter-review").click(function (e) {
        e.preventDefault();
        $(".comment-filter a").removeClass("active");
        $(this).addClass("active");
        $("#commentlist .comment").hide();
        $("#commentlist .option-review").show();
        $("#commentlist .option-review .comment").show();
    });


    // Count comments
    var numComments = $('#commentlist .comment').length;
    var numQuestions = $('#commentlist .option-question').length;
    var numReviews = $('#commentlist .option-review').length;

    // $('#comment-filter-comment').append(" (" + numComments + ")");
    $('#comment-filter-question').append(" (" + numQuestions + ")");
    // $('#comment-filter-review').append(" (" + numReviews + ")");

    //if ( numReviews > 0 ) {
    //$('.comment-count').prepend( numReviews + " Reviews | ");
    //}

    // If someone clicks recipe ratings, jump down to I Made This comments
    $(".wprm-recipe-container .wprm-recipe-rating").click(function (e) {
        e.preventDefault();

        var link = "#respond";
        $('html,body').animate({scrollTop: $(link).offset().top},'slow');
    });

    // If someone rates recipe, select 'I made this' checkbox
    //$(".wprm-rating-stars .wprm-rating-star").click(function (e) {
    $('input:radio[name="wprm-comment-rating"]').change(function(e) {
        if ($(this).val() != '0') {
            $("#comment-option-review").prop('checked', true);
        }
    });

    // Don't allow user to mark "I made this" without rating
    $(".comment-form #submit").click(function (e) {
        //if ( $('#comment-option-review').is(':checked') && $('.wprm-rating-star.rated').length == 0 ) {
        if ( $('#comment-option-review').is(':checked') && $("input[name='wprm-comment-rating']:checked").val() == 0 ) {
            e.preventDefault();
            $('.must-rate-error').css('display', 'block');
        }
    });

});
