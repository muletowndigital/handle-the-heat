jQuery(document).ready(function($) {
    // mixitup
    $('body.home #recipe_grid').mixItUp({
        load: {
            filter: '.latest' // load this tab first
        }
    });
    $('.start-here .sh-explore-bar #recipe_grid').mixItUp({
        load: {
            filter: '.bestrecipes1' // load this tab first
        }
    });
    // colorbox for recipes grid
    $('a.colourbox').colorbox({
        inline: true,
        width: "75%",
        height: "auto"
    });
    $('a.colourbox_pop').colorbox({
        inline: true,
        width: "85%",
        maxWidth: 600,
        height: "auto"
    });
    // tabs for recipe pages
    $(".tabs_wrap").show();
    $('.main#ideal').responsiveTabs({
        startCollapsed: 'false'
        // animation: 'fade'
    });
    // hide tooltip on fav stars
    $("a.wpfp-link").attr('title', '');
    // responsive iframes
    var $iframes = $("iframe");
    // Find and save the aspect ratio for all iframes
    $iframes.each(function() {
        $(this).data("ratio", this.height / this.width)
            // Remove the hardcoded width &#x26; height attributes
            .removeAttr("width").removeAttr("height");
    });

    //Fix iframe ratio on initial page load
    $iframes.each(function() {
        // Get the parent container&#x27;s width
        var width = $(this).parent().width();
        $(this).width(width).height(width * $(this).data("ratio"));
    });

    // Resize the iframes when the window is resized
    $(window).resize(function() {
        $iframes.each(function() {
            // Get the parent container&#x27;s width
            var width = $(this).parent().width();
            $(this).width(width).height(width * $(this).data("ratio"));
        });
        // Resize to fix all iframes on page load.
    }).resize();

    var slider = $("#content-slider").lightSlider({
        controls: false,
        auto: true,
        loop: true,
        item: 4,
        speed: 800,
        pause: 4000,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                item: 2,
                slideMove: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                item: 1,
                slideMove: 1
            }
        }]
    });
    $('.slideControls .slidePrev').click(function() {
        slider.goToPrevSlide();
    });
    $('.slideControls .slideNext').click(function() {
        slider.goToNextSlide();
    });

    var challengeSlider = $(".challenge-slider > .elementor-column-wrap > .elementor-widget-wrap").lightSlider({
        controls: false,
        pager: false,
        auto: true,
        loop: false,
        item: 1,
        speed: 800,
        pause: 4000,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 767,
            settings: {
                item: 1,
                slideMove: 1
            }
        }]
    });
    $('.challenge-slider__prev').click(function() {
        challengeSlider.goToPrevSlide();
    });
    $('.challenge-slider__next').click(function() {
        challengeSlider.goToNextSlide();
    });

});




