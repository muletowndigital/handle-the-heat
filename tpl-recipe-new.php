<div class="content recipe-new">

    <div class="key_info">

        <div class="container">

            <div class="key_info__inner">

                <div class="key_info__title">
                    <div class="categories"><?php the_category('  |  ') ?></div>
                    <div class="recipe-title">
                        <h1 class="fn"><?php the_title(); ?></h1>
                    </div>

                    <?php // recipe feat img, author and prep details
                    global $post;
                    $recipe = get_query_var('recipe'); ?>

                    <div class="recipe-intro-star-rating mobile">

                        <?php echo do_shortcode('[wprm-recipe-rating display="stars-details"]'); ?>
                        <div class="comment-count"><i class="fa fa-comment"></i>
                            <?php
                            $comment_count = get_comments(
                                array(
                                    'status' => 'approve',
                                    'post_id' => get_the_ID(),
                                    'type' => 'comment',
                                    'count' => true)
                            );

                            if ($comment_count == 0) {
                                echo "No Comments";
                            } else if ($comment_count == 1) {
                                echo "1 Comment";
                            } else if ($comment_count > 1) {
                                echo $comment_count . " Comments";
                            }
                            ?>
                            <i class="fa fa-chevron-down"></i>
                            <div class="comment-dropdown-options">
                                <div><a href="#respond">Leave a Review / Comment</a></div>
                                <div><a href="#commentfilter">Read Comments</a></div>
                            </div>
                        </div>
                    </div>

                    <div class="recipe-intro-star-rating desktop ">
                        <?php echo do_shortcode('[wprm-recipe-rating display="stars-details"]'); ?>
                        <div class="comment-count"><i class="fa fa-comment"></i>
                            <?php
                            $comment_count = get_comments(
                                array(
                                    'status' => 'approve',
                                    'post_id' => get_the_ID(),
                                    'type' => 'comment',
                                    'count' => true)
                            );

                            if ($comment_count == 0) {
                                echo "No Comments";
                            } else if ($comment_count == 1) {
                                echo "1 Comment";
                            } else if ($comment_count > 1) {
                                echo $comment_count . " Comments";
                            }
                            ?>
                            <i class="fa fa-chevron-down"></i>
                            <div class="comment-dropdown-options">
                                <div><a href="#respond">Leave a Review / Comment</a></div>
                                <div><a href="#commentfilter">Read Comments</a></div>
                            </div>
                        </div>
                    </div>

                    <div class="author_wrap">

                        <div class="summary">
                            <?php if ($recipe && $recipe->summary()) { ?>

                                <p><?php echo $recipe->summary(); ?></p>

                            <?php } else if (get_field('recipe_intro')) {

                                the_field('recipe_intro');

                            } else {

                                echo wp_trim_words(get_the_content(), 30, '...');

                            } ?>
                        </div>


                    </div>

                    <!-- Single Prep-->
                    <?php // yield, cook & prep time
                    global $post;
                    $recipe = get_query_var('recipe'); ?>

                    <?php if (get_field('non_recipe') != 'Yes') {

                        if ($recipe || get_field('recipe_yield') || get_field('recipe_prep') || get_field('recipe_time')) { ?>

                            <div class="prep">

                                <?php if (($recipe && $recipe->servings()) || get_field('recipe_yield')) { ?>

                                    <p><i class="fa fa-cutlery"></i> <strong>Yield:</strong> <span class="yield">

					                <?php if ($recipe && $recipe->servings()) {

                                        echo $recipe->servings() . ' ' . $recipe->servings_unit();

                                    } else {

                                        the_field('recipe_yield');

                                    } ?>

				                        </span></p>

                                <?php } ?>

                            </div>

                        <?php }

                    } ?>


                    <div class="share">
                        <!--                        --><?php //get_template_part('tpl-single-share'); ?>
                    </div>

                    <?php if (get_field('non_recipe') != 'Yes') { ?>

                        <div class="summary-buttons">


                            <?php if ($recipe) { ?>

                                <?php if (shortcode_exists('wprm-recipe-jump')) { ?>

                                    <div class="recipe_jump">

                                        <?php echo do_shortcode('[wprm-recipe-jump text="<i class=\'fas fa-utensils\'></i><span>SEE RECIPE</span>" /]'); ?>

                                    </div>

                                <?php } ?>

                                <div class="recipe_jump social-share">
                                    <?php echo do_shortcode('[socialpug_share]'); ?>
                                </div>

                                <?php if (shortcode_exists('wprm-recipe-print')) { ?>

                                    <div class="recipe_qprint recipe_jump">

                                        <?php echo do_shortcode('[wprm-recipe-print text="<i class=\'fa fa-print circle\'></i>PRINT IT"]'); ?>

                                    </div>

                                <?php } ?>

                                <div class="recipe_jump ratings_jump">
                                    <a href="#sprinkle"
                                       class="wprm-recipe-jump-to-comments wprm-recipe-link wprm-block-text-normal"><i
                                                class="fa fa-flask"></i><span>RECIPE TIPS</span>!</a>
                                </div>


                            <?php } ?>

                        </div>

                    <?php } ?>


                    <?php $pinterest = get_field('pinterest');

                    if ($pinterest) { ?>

                        <div class="pinterest_img">

                            <img src="<?php echo $pinterest['url']; ?>" alt="<?php echo $pinterest['alt'] ?: ''; ?>">

                        </div>

                    <?php } ?>

                    <?php if (get_field('recipe_cosponsored')) { ?>
                        <div class="cosponsored"><?php the_field('recipe_cosponsored'); ?></div>
                    <?php } ?>
                </div> <!-- title -->

                <div class="key_info__featured"
                     style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
                    <?php the_post_thumbnail('recipe_single', array('class' => 'photo no-lazy')); ?>
                </div>

            </div>

        </div>

    </div> <!-- key info -->

    <div class="container">

        <div class="tabs_wrap">

            <div class="main">

                <?php $recipe_rundown = get_field('recipe_rundown');
                $recipe_rundown_description = get_field('recipe_rundown_description');
                $recipe_story_left = get_field('recipe_story_left');
                $recipe_story_right = get_field('recipe_story_right');
                $comment = get_field('comment');
                $featured_comment_username = get_field('featured_comment_username');
                ?>

                <div class="recipe-contents">
                    <div class="recipe-contents__top js-contents-btn">
                        <p><?php _e('TABLE OF CONTENTS', 'handletheheat'); ?></p>
                        <i class="fas fa-chevron-circle-down"></i>
                    </div>
                    <div class="recipe-contents__contents">
                        <ul>
<!--                            <li><a href="#main-rundown">--><?php //_e('Recipe Rundown', 'hdh'); ?><!--</a></li>-->
                            <li><a href="#recipe-story"><?php _e('The Story', 'hdh'); ?></a></li>
                            <li><a href="#sprinkle"><?php _e('Sprinkle of Science', 'hdh'); ?></a></li>
                            <li><a href="#recipe"><?php _e('The Recipe', 'hdh'); ?></a></li>
                            <li><a href="#respond"><?php _e('Rate the Recipe', 'hdh'); ?></a></li>
                        </ul>
                    </div>
                </div>

                <?php if (get_field('recipe_rundown')) { ?>
                    <div id="main-rundown" class="main-rundown">
                        <div class="right-rundown-bg">
                            <div class="rundown">
                                <h3 class="rundown__title"><span>Tessa's Recipe Rundown...</span></h3>
                                <?php the_field('recipe_rundown'); ?>
                            </div>
                        </div>
                    </div> <!-- main-rundown -->
                <?php } ?>

                <?php if ($recipe_rundown_description) { ?>
                    <div class="rundown-descr">
                        <?php echo $recipe_rundown_description; ?>
                    </div>
                <?php } ?>

                <?php if ($recipe_story_left) { ?>
                    <div id="recipe-story" class="recipe-story">
                        <?php echo $recipe_story_left; ?>
                    </div>
                <?php } ?>

                <?php if ($comment) { ?>
                    <div class="featured-comment">
                        <p class="featured-comment__title"><?php _e('FEATURED COMMENT', 'handletheheat'); ?></p>
                        <p class="featured-comment__comment"><?php echo $comment; ?></p>
                        <p class="featured-comment__name"><?php echo $featured_comment_username; ?></p>

                        <div class="featured-comment__bottom">

                            <div class="recipe-intro-star-rating desktop">
                                <div class="comment-count"><i class="fa fa-comment"></i>
                                    <?php
                                    $comment_count = get_comments(
                                        array(
                                            'status' => 'approve',
                                            'post_id' => get_the_ID(),
                                            'type' => 'comment',
                                            'count' => true)
                                    );

                                    if ($comment_count == 0) {
                                        echo "No Comments";
                                    } else if ($comment_count == 1) {
                                        echo "1 Comment";
                                    } else if ($comment_count > 1) {
                                        echo $comment_count . " Comments";
                                    }
                                    ?>
                                    <div class="comment-dropdown-options">
                                        <div><a href="#respond">Leave a Review / Comment</a></div>
                                        <div><a href="#commentfilter">Read Comments</a></div>
                                    </div>
                                </div>
                            </div>

                            <div class="featured-comment__anchor">
                                <a href="#respond"><?php _e('LEAVE A COMMENT', 'handletheheat'); ?> <i
                                            class="fa fa-comment"></i></a>
                            </div>

                        </div>

                    </div>

                <?php } ?>

                <?php

                $recipe_description = get_field('recipe_description');
                $recipe_description_ad = get_field('recipe_description_ad');
                $recipe_description_image = get_field('recipe_description_image');

                ?>

                <?php if ($recipe_description || $recipe_description_ad || $recipe_description_image) { ?>
                    <div class="recipe-descr">
                        <div class="recipe-descr__top">
                            <?php if ($recipe_description) { ?>
                                <div class="recipe-descr__left">
                                    <?php echo $recipe_description; ?>
                                </div>
                            <?php } ?>
                            <?php if ($recipe_description_ad) { ?>
                                <div class="recipe-descr__right">
                                    <?php echo $recipe_description_ad; ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if ($recipe_description_image) { ?>
                            <?php echo wp_get_attachment_image($recipe_description_image['ID'], 'large', false, array('class' => '')); ?>
                        <?php } ?>
                    </div>
                <?php } ?>


                <!--          Sprinkle of Science      -->
                <?php
                $sprinkle_of_science_top_content = get_field('sprinkle_of_science_top_content');
                $sprinkle_of_science_top_left_column = get_field('sprinkle_of_science_top_left_column');
                $sprinkle_of_science_top_right_column = get_field('sprinkle_of_science_top_right_column');
                $sprinkle_of_science_middle_content_box = get_field('sprinkle_of_science_middle_content_box');
                $sprinkle_of_science_bottom_left_column = get_field('sprinkle_of_science_bottom_left_column');
                $sprinkle_of_science_bottom_right_column = get_field('sprinkle_of_science_bottom_right_column');
                $sprinkle_of_science_bottom_content_box = get_field('sprinkle_of_science_bottom_content_box');

                ?>

                <?php if ($sprinkle_of_science_top_content || $sprinkle_of_science_top_content) { ?>
                    <section id="sprinkle">
                        <div class="sprinkle">
                            <img class="sprinkle__top-img"
                                 src="<?php echo get_template_directory_uri(); ?>/images/tessas-tip.png"
                                 alt="Handle The Heat">
                            <h2 class="sprinkle__heading"><?php echo _e('Sprinkle of Science</br> Baking Tips', 'handletheheat'); ?></h2>
                            <?php if ($sprinkle_of_science_top_content) { ?>
                                <div class="sprinkle__top-cont">
                                    <?php echo $sprinkle_of_science_top_content; ?>
                                </div>
                            <?php } ?>

                            <?php if ($sprinkle_of_science_top_left_column || $sprinkle_of_science_top_right_column) { ?>
                                <div class="sprinkle__top-cols">
                                    <?php if ($sprinkle_of_science_top_left_column) { ?>
                                        <div class="sprinkle__top-left">
                                            <?php echo $sprinkle_of_science_top_left_column; ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($sprinkle_of_science_top_right_column) { ?>
                                        <div class="sprinkle__top-right">
                                            <?php echo $sprinkle_of_science_top_right_column; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>


                            <?php if ($sprinkle_of_science_middle_content_box) { ?>
                                <div class="sprinkle__middle">
                                    <?php echo $sprinkle_of_science_middle_content_box; ?>
                                </div>
                            <?php } ?>

                            <?php if ($sprinkle_of_science_bottom_left_column || $sprinkle_of_science_bottom_right_column) { ?>
                                <div class="sprinkle__top-cols">
                                    <?php if ($sprinkle_of_science_bottom_left_column) { ?>
                                        <div class="sprinkle__top-left">
                                            <?php echo $sprinkle_of_science_bottom_left_column; ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($sprinkle_of_science_bottom_right_column) { ?>
                                        <div class="sprinkle__top-right">
                                            <?php echo $sprinkle_of_science_bottom_right_column; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if ($sprinkle_of_science_bottom_content_box) { ?>
                                <div class="sprinkle__bottom">
                                    <?php echo $sprinkle_of_science_bottom_content_box; ?>
                                </div>
                            <?php } ?>
                        </div>
                    </section>
                <?php } ?>

                <!--          Recommended Products     -->
                <?php if (have_rows('recommended_products')): ?>
                    <section class="recipe-resources">
                        <h4 class="recipe-resources__heading"><?php _e('Recommended Products', 'hdh'); ?></h4>
                        <div class="recipe-resources__inner">
                            <?php while (have_rows('recommended_products')) : the_row(); ?>
                                <?php $html_code = get_sub_field('html_code'); ?>
                                <?php $link = get_sub_field('link'); ?>
                                <div class="recipe-resources__wrap">
                                <div class="recipe-resources__html"><?php echo $html_code; ?></div>
                                <?php if ($link):
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                                    <a class="recipe-resources__link"
                                       href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </div>
                    </section>
                <?php endif; ?>

                <!--         More Recipes     -->
                <?php
                $more_recipes_section = get_field('more_recipes_section');
                ?>
                <?php if ($more_recipes_section) { ?>
                    <div class="more-recipes">
                        <?php echo $more_recipes_section; ?>
                    </div>
                <?php } ?>

                <div class="r-tabs-panel r-tabs-state-active">

                    <?php $content = $post->post_content;
                    if (!has_shortcode($content, 'cft')) {
                        $print = '';
                    } else {
                        $print = 'print_friendly';
                    } ?>

                    <div class="recipe_intro <?php echo $print; ?>">
                        <?php wp_reset_query(); ?>
                        <?php the_content(); ?>
                    </div>


                    <?php if (!function_exists('dynamic_sidebar')
                        || !dynamic_sidebar('Below Post Adspace')) : ?>
                    <?php endif; ?>

                    <?php $posts = get_field('science_of_baking_recipes');
                    global $post;

                    if ($posts): ?>

                        <div class="bs-sidebar mobile-show">

                            <img src="<?php echo get_template_directory_uri(); ?>/images/baking-articles-icon.png"
                                 alt="love">

                            <h3><?php _e('Science of Baking', 'hdh'); ?> </h3>

                            <div class="related_list">

                                <?php foreach ($posts as $post): ?>

                                    <?php setup_postdata($post); ?>

                                    <div class="recipe-resources__item">

                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

                                        <a class="link recipe-resources__title"
                                           href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                    </div>

                                <?php endforeach;

                                wp_reset_postdata(); ?>

                            </div>

                        </div>

                    <?php endif; ?>

                    <?php $posts = get_field('related_recipes_picker');
                    global $post;

                    if ($posts): ?>

                        <div class="related-sidebar mobile-show">

                            <img src="<?php echo get_template_directory_uri(); ?>/images/love-icon.png" alt="love">

                            <h3><?php _e("Recipes You'll Love", 'hdh'); ?> </h3>

                            <div class="related_list">

                                <?php foreach ($posts as $post): // variable must be called $post (IMPORTANT) ?>

                                    <?php setup_postdata($post); ?>

                                    <div class="recipe-resources__item">

                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

                                        <a class="link recipe-resources__title"
                                           href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                    </div>

                                <?php endforeach;

                                wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

                            </div>

                        </div>

                    <?php endif; ?>

                    <div class="comments_wrap">
                        <?php comments_template('', true); ?>
                    </div>

                </div>

                <div class="print_only copyright">
                    <p>&copy; Handle the Heat - handletheheat.com</p>
                </div>

            </div>

            <div class="sidebar launch ">
                <?php
                $hide = get_field('hide_section');
                $recipe_cta_image = get_field('recipe_cta_image');
                $recipe_cta_text = get_field('recipe_cta_text');
                $recipe_cta_button = get_field('recipe_cta_button');
                $bg = get_field('background_image');
                $advertisement_1 = get_field('advertisement_1');
                $advertisement_2 = get_field('advertisement_2');
                $advertisement_3 = get_field('advertisement_3');
                ?>
                <?php if ($recipe_cta_image || $recipe_cta_text || $recipe_cta_button) { ?>
                    <div class="cta-sidebar" style="background-image: url('<?php echo $bg['url'] ?>')">

                        <?php if ($recipe_cta_text || $recipe_cta_button) { ?>
                            <div class="cta-sidebar__right">
                                <?php if ($recipe_cta_text) { ?>
                                    <?php echo $recipe_cta_text; ?>
                                <?php } ?>

                                <?php
                                $link_url = $recipe_cta_button['url'];
                                $link_title = $recipe_cta_button['title'];
                                $link_target = $recipe_cta_button['target'] ? $recipe_cta_button['target'] : '_self'; ?>

                                <?php if ($recipe_cta_image) { ?>
                                    <div class="cta-sidebar__image"><a
                                                href="<?php echo $link_url; ?>"><?php echo wp_get_attachment_image($recipe_cta_image['ID'], 'medium', false, array('class' => '')); ?></a>
                                    </div>
                                <?php } ?>

                                <?php if ($recipe_cta_button): ?>
                                    <a class="cta-recipe__btn"
                                       href="<?php echo esc_url($link_url); ?>"
                                       target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                                <?php endif; ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($advertisement_1) { ?>
                    <div class="mobile-hidden">    <?php echo $advertisement_1; ?>
                    </div>
                <?php } ?>

                <?php
                $hide = get_field('hide_section');
                ?>

                <div class="contentabout">
                    <div class="contentabout__about">
                        <div class="contentabout__avatar">
                            <img alt="Tessa Arias"
                                 src='<?php echo get_template_directory_uri(); ?>/images/people/tessa-gravatar.jpeg'
                                 width="165" height="165">
                        </div>
                        <h3>About <?php the_author_meta('first_name'); ?>...</h3>

                        <?php
                        $facebook_user = get_the_author_meta('facebook');
                        $pinterest_user = get_the_author_meta('pinterest');
                        $youtube_user = get_the_author_meta('youtube');
                        $instagram_user = get_the_author_meta('instagram');
                        $twitter_user = get_the_author_meta('twitter');
                        ?>

                        <p><?php the_author_meta('description'); ?></p>

                        <div class="contentabout__socials">
                            <?php if (!empty($facebook_user)) { ?>
                                <a href="<?php echo the_author_meta('facebook'); ?>"><i
                                            class="fa fa-facebook circle"></i></a>
                            <?php } ?>
                            <?php if (!empty($pinterest_user)) { ?>
                                <a href="<?php echo the_author_meta('pinterest'); ?>"><i
                                            class="fa fa-pinterest circle"></i></a>
                            <?php } ?>
                            <?php if (!empty($youtube_user)) { ?>
                                <a href="<?php echo the_author_meta('youtube'); ?>"><i
                                            class="fa fa-youtube-play circle"></i></a>
                            <?php } ?>
                            <?php if (!empty($instagram_user)) { ?>
                                <a href="<?php echo the_author_meta('instagram'); ?>"><i
                                            class="fa fa-instagram circle"></i></a>
                            <?php } ?>
                            <?php if (!empty($twitter_user)) { ?>
                                <a href="https://twitter.com/<?php echo the_author_meta('twitter'); ?>"><i
                                            class="fa fa-twitter circle"></i></a>
                            <?php } ?>
                        </div>

                    </div> <!-- rundown-right -->
                </div> <!-- main-rundown -->

                <?php if ($advertisement_2) { ?>
                    <div class="mobile-hidden"><?php echo $advertisement_2; ?></div>

                <?php } ?>

                <?php $posts = get_field('science_of_baking_recipes');
                global $post;

                if ($posts): ?>

                    <div class="bs-sidebar mobile-hidden">

                        <img src="<?php echo get_template_directory_uri(); ?>/images/baking-articles-icon.png"
                             alt="love">

                        <h3><?php _e('Science of Baking', 'hdh'); ?> </h3>

                        <div class="related_list">

                            <?php foreach ($posts as $post): ?>

                                <?php setup_postdata($post); ?>

                                <div class="recipe-resources__item">

                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

                                    <a class="link recipe-resources__title"
                                       href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                </div>

                            <?php endforeach;

                            wp_reset_postdata(); ?>

                        </div>

                    </div>

                <?php endif; ?>

                <?php $posts = get_field('related_recipes_picker');
                global $post;

                if ($posts): ?>

                    <div class="related-sidebar mobile-hidden">

                        <img src="<?php echo get_template_directory_uri(); ?>/images/love-icon.png" alt="love">

                        <h3><?php _e("Recipes You'll Love", 'hdh'); ?> </h3>

                        <div class="related_list">

                            <?php foreach ($posts as $post): ?>

                                <?php setup_postdata($post); ?>

                                <div class="recipe-resources__item">

                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

                                    <a class="link recipe-resources__title"
                                       href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                                </div>

                            <?php endforeach;

                            wp_reset_postdata(); ?>

                        </div>

                    </div>

                <?php endif; ?>

                <?php if ($advertisement_3) { ?>
                    <div class="mobile-hidden"><?php echo $advertisement_3; ?>
                    </div>
                <?php } ?>
            </div>

        </div>


        <?php
        $recipe_cta_image = get_field('recipe_cta_image');
        $recipe_cta_text = get_field('recipe_cta_text');
        $recipe_cta_button = get_field('recipe_cta_button');; ?>

        <?php if ($recipe_cta_image || $recipe_cta_text || $recipe_cta_button) { ?>
            <div class="cta-recipe mobile-hidden sticky-stopper">
                <div class="container">
                    <?php if ($recipe_cta_image) { ?>
                        <div class="cta-recipe__image"><?php echo wp_get_attachment_image($recipe_cta_image['ID'], 'medium', false, array('class' => '')); ?></div>
                    <?php } ?>
                    <?php if ($recipe_cta_text || $recipe_cta_button) { ?>
                        <div class="cta-recipe__right">
                            <?php if ($recipe_cta_text) { ?>
                                <?php echo $recipe_cta_text; ?>
                            <?php } ?>

                            <?php if ($recipe_cta_button):
                                $link_url = $recipe_cta_button['url'];
                                $link_title = $recipe_cta_button['title'];
                                $link_target = $recipe_cta_button['target'] ? $recipe_cta_button['target'] : '_self'; ?>
                                <a class="cta-recipe__btn"
                                   href="<?php echo esc_url($link_url); ?>"
                                   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

    </div>

    <div class="recipe-categories">
        <?php echo do_shortcode('[elementor-template id="52510"]'); ?>
    </div>


</div>
