<?php $posts = get_field( 'related_recipes_picker' );
global $post;

if( $posts ): ?>

	<div class="related">

		<h4>You&#8217;ll Also Love <br/> <hr/> </h4>

	    <ul class="related_list">

		    <?php foreach( $posts as $post ): // variable must be called $post (IMPORTANT) ?>

		        <?php setup_postdata( $post ); ?>

		        <li>

		            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>

		            <a class="link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

		        </li>

		    <?php endforeach;

		    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

	    </ul>

	</div>

<?php endif;