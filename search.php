<?php 
	// search results for recipes
?>

<?php get_header(); ?>

	<div class="content">
		
		<div class="container">	

			<div class="main">
				<?php //get_template_part('tpl-searchgrid'); ?>

<div class="recipe_search">
		
		<h2><i class="fa fa-tags"></i>Find a Recipe</h2>

		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<input type="search" id="s" name="s" placeholder="Search recipe names...">
			<i class="fa fa-search"></i>					        
		    <input type="submit" value="" id="searchsubmit" />
		</form>

		<?php echo do_shortcode('[searchandfilter id="9569"]'); ?>

	</div>


<ul id="search-recipe-grid">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<li class="mix">
				<?php get_template_part('tpl-recipethumb'); ?>
			</li>

		<?php endwhile; endif; ?>

		<div class="pagination">

			<?php global $wp_query;
				$big = 999999999; // need an unlikely integer
				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages
				) );
			?>

		</div>	

	</ul>


			</div>			

			<div class="sidebar launch">
				<?php get_sidebar('launch'); ?>
			</div>

		</div>

	</div>

<?php get_footer(); ?>
