<?php echo do_shortcode('[socialpug_share]'); ?>
<?php if ( $recipe && $recipe->id() ) { ?>
    <div class="share-print-button-wrapper">
        <a href="/wprm_print/<?php echo $recipe->id();?>" target="_blank">
            <div class="share-print-button">
                <i class="fa fa-print circle"></i>
            </div>
        </a>
    </div>
<?php }