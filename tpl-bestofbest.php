<div>
<a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) { ?>				
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'teaser' ); $url = $thumb['0']; ?>
			<img src="<?php echo $url; ?>" alt="<?php the_title(); ?>" />
		<?php } else { ?>
			<div style="background:url(<?php echo catch_that_image(); ?>) no-repeat 50% -5px; background-size:cover;">
				<img src="<?php echo get_template_directory_uri(); ?>/images/fallback-trans.png">
			</div>
		<?php } ?>							
	</a>
</div>
<div><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>