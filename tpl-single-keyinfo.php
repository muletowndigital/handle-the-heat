<?php // recipe feat img, author and prep details
global $post;
$recipe = get_query_var( 'recipe' ); ?>

<div class="recipe-intro-star-rating mobile">
	
	<?php echo do_shortcode( '[wprm-recipe-rating display="stars-details"]' ); ?>
    <div class="comment-count"><i class="fa fa-comment"></i>
		<?php
			$comment_count =  get_comments(
								array(
								'status' => 'approve',
								'post_id'=> get_the_ID(), 
								'type'=> 'comment', 
								'count' => true)
							 );
		
			if ( $comment_count == 0 ) {
				echo "No Comments";
			} else if ( $comment_count == 1 ) {
				echo "1 Comment";
			} else if ( $comment_count > 1 ) {
				echo $comment_count . " Comments";
			}
		?>
        <i class="fa fa-chevron-down"></i>
        <div class="comment-dropdown-options">
            <div><a href="#respond">Leave a Review / Comment</a></div>
            <div><a href="#commentfilter">Read Comments</a></div>
        </div>
    </div>
</div>

<!--<div class="author_wrap mobile">
    <div class="summary">

        <?php if ( $recipe && $recipe->summary() ) { ?>

            <p>

                <?php echo $recipe->summary(); ?>

            </p>

        <?php }

        else if ( get_field( 'recipe_intro' ) ) {

            the_field( 'recipe_intro' );

        }

        else {

            echo wp_trim_words( get_the_content(), 30, '...' );

        } ?>

    </div>
</div> -->



<div class="recipe-intro-star-rating desktop">
	<?php //ho do_shortcode( '[wprm-recipe-rating display="stars"]' ); ?>
	<?php echo do_shortcode( '[wprm-recipe-rating display="stars-details"]' ); ?>
    <div class="comment-count"><i class="fa fa-comment"></i>
	    <?php
			$comment_count =  get_comments(
								array(
								'status' => 'approve',
								'post_id'=> get_the_ID(), 
								'type'=> 'comment', 
								'count' => true)
							 );
		
			if ( $comment_count == 0 ) {
				echo "No Comments";
			} else if ( $comment_count == 1 ) {
				echo "1 Comment";
			} else if ( $comment_count > 1 ) {
				echo $comment_count . " Comments";
			}
		?>
        <i class="fa fa-chevron-down"></i>
        <div class="comment-dropdown-options">
            <div><a href="#respond">Leave a Review / Comment</a></div>
            <div><a href="#commentfilter">Read Comments</a></div>
        </div>
    </div>
</div>




<div class="author_wrap">

	

    <?php if ( get_field( 'non_recipe' ) != 'Yes' ) { ?>

        <div class="summary-buttons">

			

			<?php if ( $recipe ) { ?>

				<?php if ( shortcode_exists( 'wprm-recipe-jump' ) ) { ?>

					<div class="recipe_jump">

						<?php echo do_shortcode( '[wprm-recipe-jump text="<i class=\'fa fa-chevron-down\'></i> <span>Jump to </span>Recipe!" /]' ); ?>

					</div>

				<?php } ?>

				<div class="recipe_jump ratings_jump">
                    <a href="#respond" class="wprm-recipe-jump-to-comments wprm-recipe-link wprm-block-text-normal"><i class="fa fa-chevron-down"></i> Rate<span> this Recipe</span>!</a>
                </div>

                <?php if ( shortcode_exists( 'wprm-recipe-jump-video' ) ) { ?>

				     <div class="recipe_jump recipe_jump_video">

						<?php echo do_shortcode( '[wprm-recipe-jump-video text="<i class=\'fa fa-video-camera\'></i> <span>Watch </span>Video" /]' ); ?>

					</div>

				<?php } ?>

				<?php //if ( shortcode_exists( 'wprm-recipe-print' ) ) { ?>

					 <!-- <div class="recipe_qprint">

						<?php echo do_shortcode( '[wprm-recipe-print text="<i class=\'fa fa-print circle\'></i>"]' ); ?>

					</div> -->

				<?php // }

				 

			} ?>

		</div>

	<?php } ?>

	<div class="summary">
		<?php if ( $recipe && $recipe->summary() ) { ?>

			<p><?php echo $recipe->summary(); ?></p>

		<?php }

		else if ( get_field( 'recipe_intro' ) ) {

			the_field( 'recipe_intro' );

		} else {

			echo wp_trim_words( get_the_content(), 30, '...' );

		} ?>
	</div> 

	

</div>

<?php get_template_part( 'tpl-single-prep' ); ?>

<div class="share">
	<?php get_template_part('tpl-single-share'); ?>
	</div>

<?php $pinterest = get_field( 'pinterest' );

if ( $pinterest ) { ?>

	<div class="pinterest_img">

		<img src="<?php echo $pinterest['url']; ?>" alt="<?php echo $pinterest['alt'] ?: ''; ?>">

	</div>

<?php }