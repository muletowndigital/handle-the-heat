<?php
// Look for the NEW version of a specific POST before we do ANYTHING else in the code.

if (is_single(55625)) {
    $random_var = rand(1, 10);
    if (($random_var % 2) == 0) {
        // Do Nothing, keep them here
    } else {
        // Send them to the old post (secondary post)
        header('Location: /pumpkin-spice-coffee-cake/');
        exit;
    }
}
?>

<?php get_header(); ?>

<?php if (get_field('recipe_prep_time_group') == '15') {
    $preptime = "PT15M";
} elseif (get_field('recipe_prep_time_group') == '30') {
    $preptime = "PT30M";
} elseif (get_field('recipe_prep_time_group') == '45') {
    $preptime = "PT45M";
} elseif (get_field('recipe_prep_time_group') == '60') {
    $preptime = "PT1H00M";
} elseif (get_field('recipe_prep_time_group') == '99') {
    $preptime = "PT1H30M";
} ?>
<?php if (get_field('recipe_time_group') == '15') {
    $cooktime = "PT15M";
} elseif (get_field('recipe_time_group') == '30') {
    $cooktime = "PT30M";
} elseif (get_field('recipe_time_group') == '45') {
    $cooktime = "PT45M";
} elseif (get_field('recipe_time_group') == '60') {
    $cooktime = "PT1H00M";
} elseif (get_field('recipe_time_group') == '120') {
    $cooktime = "PT2H00M";
} elseif (get_field('recipe_time_group') == '180') {
    $cooktime = "PT3H00M";
} elseif (get_field('recipe_time_group') == '240') {
    $cooktime = "PT4H00M";
} ?>
<?php
$ingredients = get_field('recipe_ingredients');
$ingredients1 = str_replace('<p>', ' ', $ingredients);
$ingredients2 = str_replace('</p>', ' ', $ingredients1);

$ingredients3 = str_replace('<br />', ' " , " ', $ingredients2);

global $post;

if (class_exists('WPRM_Recipe_Manager')) {
    $recipe_id = 0;

    // Get first recipe in post content.
    if (!$recipe_id) {

        $parent_post = get_post();
        $recipes = WPRM_Recipe_Manager::get_recipe_ids_from_content($parent_post->post_content);

        if (isset($recipes[0])) {

            $recipe_id = $recipes[0];

        }

    }

    $recipe = WPRM_Recipe_Manager::get_recipe($recipe_id);

}

set_query_var('recipe', $recipe); ?>


<!--New Recipe Layout-->
<?php $new_recipe_layout = get_field('new_recipe_layout'); ?>
<?php if ($new_recipe_layout) { ?>
    <?php get_template_part('tpl-recipe-new'); ?>
<?php } else { ?>

    <!--Old Recipe Layout-->
    <div class="content">

        <div class="container">

            <div class="key_info">
                <div class="featured">
                    <?php the_post_thumbnail('recipe_single', array('class' => 'photo no-lazy')); ?>
                </div>
                <div class="title">
                    <!--<div class="key_info_inner"> -->

                    <div class="categories">Filed Under: <?php the_category('  |  ') ?></div>
                    <div class="recipe-title">
                        <h1 class="fn"><?php the_title(); ?></h1>
                    </div>
                    <div class="authordate">
                        <div class="author">
                            By <?php echo get_the_author_meta('display_name', get_post_field('post_author', $post_id)); ?> </div>
                        &nbsp; | &nbsp;
                        <div class="date"><?php echo get_the_date('F jS, Y', $post_id); ?></div>
                    </div>
                    <!-- </div> -->
                    <?php get_template_part('tpl-single-keyinfo'); ?>
                    <?php if (get_field('recipe_cosponsored')) { ?>
                        <div class="cosponsored"><?php the_field('recipe_cosponsored'); ?></div>
                    <?php } ?>

                </div> <!-- title -->
            </div> <!-- key info -->

            <?php if (get_field('recipe_rundown')) { ?>
                <div class="main-rundown">
                    <div class="left-rundown"><img
                                src="<?php echo get_template_directory_uri(); ?>/images/tessa-recipe-rundown.jpg"
                                width="200" height="200"/></div>
                    <div class="right-rundown-bg">
                        <div class="right-rundown">
                            <div class="rundown ">
                                <h3>Tessa's Recipe Rundown...</h3>
                                <p>
                                    <?php the_field('recipe_rundown'); ?>
                                </p>
                            </div>
                        </div> <!-- rundown-right -->
                    </div>
                </div> <!-- main-rundown -->
            <?php } ?>

            <div class="tabs_wrap">

                <div class="main">

                    <div class="r-tabs-panel r-tabs-state-active">

                        <?php $content = $post->post_content; // get the post content
                        if (!has_shortcode($content, 'cft')) { // only print if has shortcode and recipe dets are outputted within the_content
                            $print = '';
                        } else {
                            $print = 'print_friendly';
                        } ?>

                        <div class="recipe_intro <?php echo $print; ?>">
                            <?php wp_reset_query(); ?>
                            <?php the_content(); ?>
                        </div>


                        <?php
                        get_template_part('tpl', 'related-products');
                        get_template_part('tpl', 'about-author');
                        get_template_part('tpl', 'related-recipes'); ?>


                        <!--<div class="cookbook-img-above-about">
                            <div data-capture-embed='nq0iO'></div>
                        </div>-->


                        <?php if (!function_exists('dynamic_sidebar')
                            || !dynamic_sidebar('Below Post Adspace')) : ?>
                        <?php endif; ?>

                        <div class="comments_wrap">
                            <?php comments_template('', true); ?>
                        </div>


                    </div>

                    <div class="print_only copyright">
                        <p>&copy; Handle the Heat - handletheheat.com</p>
                    </div>

                </div>

                <div class="sidebar launch">
                    <?php get_sidebar('launch'); ?>
                </div>

            </div>

            <div class="single-cookie-handbook">
                <h4>Order your Ultimate Cookie Handbook Hardcopy! <br>
                    <hr/>
                </h4>
                <div class="single-cookiehandbook-bg">
                    <div class="single-cookiehandbook-box">
                        <div class="single-cookie-handbook-left"><img
                                    src="<?php echo get_template_directory_uri(); ?>/images/the-ultimate-cookie-handbook-blog.jpg"
                                    alt="The Ultimate Cookie Handbook" width="360" height="360"/></div>
                        <div class="single-cookie-handbook-right">Over 200 pages with 50+ cookie recipes that'll make
                            you a COOKIE PRO. Discover how to turn your biggest cookie flops into WINS by mastering the
                            sweet science of baking. Even learn how to customize your own recipes! Beautiful, hardcopy,
                            full color, photos of every recipe so you know EXACTLY how your cookies should look. Order
                            now to have the book delivered to your doorstep!<br/>
                            <div class="buybook"><span><a href="https://www.handletheheat.com/cookbook" target="_blank">Buy Now</a></span>
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- single-cookie-handbook -->


            <div class="single-join-hth">
                <h4>Join the Handle the Heat Community <br>
                    <hr/>
                </h4>
                <div class="single-join-hth-bg">
                    <div class="single-joinhth-box">
                        <div class="single-joinhth-left"><img
                                    src="<?php echo get_template_directory_uri(); ?>/images/join-hth-cookie-customization.png"
                                    alt="Cookie Customization Chart" width="227" height="300"/></div>
                        <div class="single-joinhth-right"><strong>Do you want a more delicious life? </strong><br/>
                            Instead of digging through cookbooks and magazines and searching the internet for amazing
                            recipes, subscribe to Handle the Heat to receive new recipe posts delivered straight to your
                            email inbox. You’ll get all the latest recipes, videos, kitchen tips and tricks AND my
                            *free* Cookie Customization Guide (because I am the Cookie Queen)! <br/>
                            <div class="signup"><span><a href="https://handletheheat.com/cookie-cheatsheet"
                                                         target="_blank">Sign Up</a></span></div>
                        </div>
                    </div>
                </div>
            </div> <!-- single-join-hth -->
        </div>
    </div>

<?php }; ?>
<?php get_footer(); ?>
