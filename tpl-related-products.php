<?php if( have_rows( 'recipe_product' ) ): ?>

	<div class="related">
		<h4 style="margin-bottom:10px;">For this recipe I recommend</h4>

		<p style="text-align:center; font-size:13px; padding-bottom:10px;">affiliate links</p>

	    <ul class="related_list">

			<?php while ( have_rows( 'recipe_product' ) ) : the_row(); ?>

				<li>

		            <a rel="nofollow" target="_blank" href="<?php the_sub_field( 'product_link' ); ?>">

						<img src="<?php the_sub_field( 'product_image' ); ?>" alt="<?php the_sub_field( 'product_name' ); ?>">

		            </a>

		            <a rel="nofollow" target="_blank" href="<?php the_sub_field( 'product_link' ); ?>"><?php the_sub_field( 'product_name' ); ?></a>

				</li>

			<?php endwhile; ?>

		</ul>

	</div>

<?php endif;