<?php 
	// search results for blog posts - same as index.php except for the query_posts
?>

<?php get_header(); ?>

	<div class="content">
		
		<div class="container">
					
			<div class="main">

				<h2 class="title">Blog</h2>

				<?php echo do_shortcode('[searchandfilter id="9571"]'); ?>

				<?php get_template_part('tpl-blog'); ?>			

			</div>

			<div class="sidebar launch">
				<?php get_sidebar('launch'); ?>
			</div>

		</div>

	</div>

<?php get_footer(); ?>