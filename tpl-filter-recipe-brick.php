<?php
// displays the recipe thumbnail and fave, pinterest links
// used on the homepage, faves page and anywhere tpl-searchgrid is used
?>
<div class="recipe-titlebar">
    <div class="recipe-title"><a target="_blank" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
</div>
<a target="_blank" href="<?php the_permalink(); ?>">

    <?php if ( has_post_thumbnail() ) { ?>
        <div class="recipe-image">
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'filter-recipe' ); $url = $thumb['0']; ?>
            <?php $title = get_the_title() ;?>
            <?php $alt = wp_strip_all_tags($title) ;?>
            <img src="<?php echo $url; ?>" alt="<?php echo $alt ;?>" />
        </div>
    <?php } else { ?>
        <div style="background:url(<?php echo catch_that_image(); ?>) no-repeat 50% -5px; background-size:cover;">
            <img src="<?php echo get_template_directory_uri(); ?>/images/fallback-trans.png">
        </div>
    <?php } ?>
</a>




