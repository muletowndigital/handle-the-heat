<?php 
	// yield, cook & prep time
?>

	<?php if ( (get_field('recipe_yield')) || (get_field('recipe_prep')) || (get_field('recipe_time')) ) { ?>

<div class="whole-recipe-prepbox">
<div class="whole-recipe-yield">
<?php if ( get_field('recipe_yield') ) { ?>
<i class="fa fa-cutlery"></i> <strong>Yield:</strong> <span class="yield"><?php the_field('recipe_yield'); ?></span>
<?php } ?>
</div>

<div class="whole-recipe-prepcook">
<?php if ( get_field('recipe_prep') ) { ?>
				
					<div><i class="fa fa-clock-o"></i> 
					<strong>Prep Time:</strong> 
					<?php if (get_field('recipe_prep_time_group') == '15') { ?>
						<span class="preptime"><span class="value-title" title="PT15M"></span><?php the_field('recipe_prep'); ?></span>
					<?php } else if (get_field('recipe_prep_time_group') == '30') { ?>
						<span class="preptime"><span class="value-title" title="PT30M"></span><?php the_field('recipe_prep'); ?></span>
					<?php } else if (get_field('recipe_prep_time_group') == '45') { ?>
						<span class="preptime"><span class="value-title" title="PT45M"></span><?php the_field('recipe_prep'); ?></span>
					<?php } else if (get_field('recipe_prep_time_group') == '60') { ?>
						<span class="preptime"><span class="value-title" title="PT1H00M"></span><?php the_field('recipe_prep'); ?></span>
					<?php } else if (get_field('recipe_prep_time_group') == '99') { ?>
						<span class="preptime"><span class="value-title" title="PT1H00M"></span><?php the_field('recipe_prep'); ?></span>
					<?php } else { ?>
						<?php the_field('recipe_prep'); ?>
					<?php }	?>
				</div>
			<?php } ?>




<?php if ( get_field('recipe_time') ) { ?>
				
					<div><i class="fa fa-clock-o"></i> 
					<strong>Cook:</strong> 
					<?php if (get_field('recipe_time_group') == '15') { ?>
						<span class="duration"><span class="value-title" title="PT15M"></span><?php the_field('recipe_time'); ?></span>
					<?php } else if (get_field('recipe_time_group') == '30') { ?>
						<span class="duration"><span class="value-title" title="PT30M"></span><?php the_field('recipe_time'); ?></span>
					<?php } else if (get_field('recipe_time_group') == '45') { ?>
						<span class="duration"><span class="value-title" title="PT45M"></span><?php the_field('recipe_time'); ?></span>
					<?php } else if (get_field('recipe_time_group') == '60') { ?>
						<span class="duration"><span class="value-title" title="PT1H00M"></span><?php the_field('recipe_time'); ?></span>
					<?php } else if (get_field('recipe_time_group') == '120') { ?>
						<span class="duration"><span class="value-title" title="PT2H00M"></span><?php the_field('recipe_time'); ?></span>
					<?php } else if (get_field('recipe_time_group') == '180') { ?>
						<span class="duration"><span class="value-title" title="PT3H00M"></span><?php the_field('recipe_time'); ?></span>
					<?php } else if (get_field('recipe_time_group') == '240') { ?>
						<span class="duration"><span class="value-title" title="PT4H00M"></span><?php the_field('recipe_time'); ?></span>
					<?php } else { ?>
						<?php the_field('recipe_time'); ?>
					<?php }	?>
				</div>
			<?php } ?>
</div>
</div>


	
	<?php } ?>

