<?php
/**
 * Handle the Heat recipe template.
 *
 * @author     Lauren Gray, Once Coupled <lauren@oncecoupled.com>
 * @since      1.0.0
 *
 * @package    WP_Recipe_Maker
 */

// @codingStandardsIgnoreStart
?>
<?php $new_recipe_layout = get_field('new_recipe_layout'); ?>

<?php if ($new_recipe_layout) { ?>
    <?php
    /**
     * Handle the Heat recipe template.
     *
     * @author     Lauren Gray, Once Coupled <lauren@oncecoupled.com>
     * @since      1.0.0
     *
     * @package    WP_Recipe_Maker
     */

// @codingStandardsIgnoreStart
    ?>

    <div class="wprm-recipe wprm-recipe-handle-the-heat wprm-whole-recipe">

        <div class="wprm-recipe-image featured">[wprm-recipe-image default="small"]</div>


        <div class="wprm-entry-header">

            <h2 class="wprm-recipe-name">
                <div class="how">How to make</div>
                <div class="how_title">[wprm-recipe-name]</div>
            </h2>

            <!-- [wprm-recipe-rating display="stars"] -->
            [wprm-recipe-rating display="stars-details"]


            <div class="wprm-recipe-details-container whole-recipe-prepbox">

                <?php if ($recipe->servings() || $recipe->total_time()) : ?>

                    <div class="whole-recipe-yield">

                        <!--                    --><?php //if ($recipe->total_time()) : ?>
                        <!---->
                        <!--                        <div class="wprm-recipe-total-time-container">-->
                        <!--                            <i class="fa fa-clock-o"></i> <strong-->
                        <!--                                    class="wprm-recipe-details-name wprm-recipe-total-time-name">-->
                        <?php //echo WPRM_Template_Helper::label('total_time'); ?><!--</strong>-->
                        <!--                            [wprm-recipe-time type="total"]-->
                        <!--                        </div>-->
                        <!---->
                        <!--                    --><?php //endif; // Total time. ?>

                        <?php if ($recipe->servings()) : ?>
                            <div class="wprm-recipe-yield-container">
                                <i class="fa fa-cutlery"></i> <strong>Yield:</strong> <span
                                        class="wprm-recipe-details wprm-recipe-servings yield"><?php echo $recipe->servings(); ?></span>
                                <span class="wprm-recipe-details-unit wprm-recipe-servings-unit"><?php echo $recipe->servings_unit(); ?></span>
                            </div>
                        <?php endif; ?>

                    </div>

                <?php endif; // Servings. ?>

                <?php if ($recipe->prep_time() || $recipe->cook_time() || ($recipe->custom_time() && $recipe->custom_time_label()) || $recipe->total_time()) : ?>

                    <div class="whole-recipe-prepcook">

                        <?php if ($recipe->prep_time()) : ?>

                            <div class="wprm-recipe-prep-time-container">
                                <i class="fa fa-clock-o"></i> <strong
                                        class="wprm-recipe-details-name wprm-recipe-prep-time-name"><?php echo WPRM_Template_Helper::label('prep_time'); ?></strong>
                                [wprm-recipe-time type="prep"]
                            </div>

                        <?php endif; // Prep time. ?>

                        <?php if ($recipe->cook_time()) : ?>

                            <div class="wprm-recipe-cook-time-container">
                                <i class="fa fa-clock-o"></i> <strong
                                        class="wprm-recipe-details-name wprm-recipe-cook-time-name"><?php echo WPRM_Template_Helper::label('cook_time'); ?></strong>
                                [wprm-recipe-time type="cook"]
                            </div>

                        <?php endif; // Cook time. ?>

                        <?php if ($recipe->custom_time() && $recipe->custom_time_label()) : ?>

                            <div class="wprm-recipe-custom-time-container">
                                <i class="fa fa-clock-o"></i> <strong
                                        class="wprm-recipe-details-name wprm-recipe-custom-time-name"><?php echo $recipe->custom_time_label(); ?></strong>
                                [wprm-recipe-time type="custom"]
                            </div>

                        <?php endif; // Custom time. ?>

                    </div>

                <?php endif; // Times. ?>

            </div>


            <?php if (get_field('non_recipe') != 'Yes') { ?>

                <div class="summary-buttons wprm-summary-buttons">


                    <?php if ($recipe) { ?>

                        <div class="recipe_jump social-share">
                            <?php echo do_shortcode('[socialpug_share]'); ?>
                        </div>

                        <?php if (shortcode_exists('wprm-recipe-print')) { ?>

                            <div class="recipe_qprint recipe_jump">

                                <?php echo do_shortcode('[wprm-recipe-print text="<i class=\'fa fa-print circle\'></i>PRINT IT"]'); ?>

                            </div>

                        <?php } ?>

                        <!--                    <div class="recipe_jump ratings_jump">-->
                        <!--                        <a href="#" class="wprm-recipe-jump-to-comments wprm-recipe-link wprm-block-text-normal"><i-->
                        <!--                                    class="fas fa-bookmark"></i><span>SAVE IT</span>!</a>-->
                        <!--                    </div>-->


                    <?php } ?>

                </div>

            <?php } ?>

            <?php if ($recipe->summary() && '<br>' !== $recipe->summary()) { ?>

                <div class="wprm-recipe-summary">[wprm-recipe-summary]</div>

            <?php } ?>

        </div>

        <div class="wprm-entry-content">

            <?php $ingredients = $recipe->ingredients();
            if (count($ingredients) > 0) : ?>

                <div class="wprm-recipe-ingredients-container ingredients">

                    <h3 class="wprm-recipe-header title"><?php echo WPRM_Template_Helper::label('ingredients'); ?></h3>

                    <?php foreach ($ingredients as $ingredient_group) : ?>

                        <div class="wprm-recipe-ingredient-group">

                            <?php if ($ingredient_group['name']) : ?>

                                <h4 class="wprm-recipe-group-name wprm-recipe-ingredient-group-name"><?php echo $ingredient_group['name']; ?></h4>

                            <?php endif; // Ingredient group name. ?>

                            <ul class="wprm-recipe-ingredients ingredient">

                                <?php foreach ($ingredient_group['ingredients'] as $ingredient) : ?>

                                    <li class="wprm-recipe-ingredient">

                                        <?php if ($ingredient['amount']) : ?>

                                            <span class="wprm-recipe-ingredient-amount"><?php echo $ingredient['amount']; ?></span>

                                        <?php endif; // Ingredient amount. ?>

                                        <?php if ($ingredient['unit']) : ?>

                                            <span class="wprm-recipe-ingredient-unit"><?php echo $ingredient['unit']; ?></span>

                                        <?php endif; // Ingredient unit. ?>

                                        <span class="wprm-recipe-ingredient-name"><?php echo WPRM_Template_Helper::ingredient_name($ingredient, true); ?></span>

                                        <?php if ($ingredient['notes']) : ?>

                                            <span class="wprm-recipe-ingredient-notes"><?php echo $ingredient['notes']; ?></span>

                                        <?php endif; // Ingredient notes. ?>

                                    </li>

                                <?php endforeach; // Ingredients. ?>

                            </ul>

                            <div class="add-placeholder">
                            </div>

                        </div>

                    <?php endforeach; // Ingredient groups. ?>

                    <?php echo WPRM_Template_Helper::unit_conversion($recipe); ?>

                </div>

            <?php endif; // Ingredients. ?>

            <?php $instructions = $recipe->instructions();
            if (count($instructions) > 0) : ?>

                <div class="wprm-recipe-instructions-container directions">

                    <h3 class="wprm-recipe-header title"><?php echo WPRM_Template_Helper::label('instructions'); ?></h3>

                    <?php foreach ($instructions as $instruction_group) : ?>

                        <div class="wprm-recipe-instruction-group">

                            <?php if ($instruction_group['name']) : ?>

                                <h4 class="wprm-recipe-group-name wprm-recipe-instruction-group-name"><?php echo $instruction_group['name']; ?></h4>

                            <?php endif; // Instruction group name. ?>

                            <ol class="wprm-recipe-instructions instructions">

                                <?php foreach ($instruction_group['instructions'] as $instruction) : ?>

                                    <li class="wprm-recipe-instruction">

                                        <?php if ($instruction['text']) : ?>

                                            <div class="wprm-recipe-instruction-text"><?php echo $instruction['text']; ?></div>

                                        <?php endif; // Instruction text. ?>

                                        <?php if ($instruction['image']) : ?>

                                            <div class="wprm-recipe-instruction-image"><?php echo WPRM_Template_Helper::instruction_image($instruction, 'thumbnail'); ?></div>

                                        <?php endif; // Instruction image. ?>

                                    </li>

                                <?php endforeach; // Instructions. ?>

                            </ol>

                        </div>

                    <?php endforeach; // Instruction groups. ?>

                </div>

            <?php endif; // Instructions. ?>


            <!--        --><?php //if ($recipe->video()) : ?>
            <!---->
            <!--            <div class="wprm-recipe-video-container">-->
            <!---->
            <!--                <h3 class="wprm-recipe-header">-->
            <?php //echo WPRM_Template_Helper::label('video'); ?><!--</h3>-->
            <!---->
            <!--                [wprm-recipe-video]-->
            <!---->
            <!--            </div>-->
            <!---->
            <!--        --><?php //endif; ?>

            <?php if ($recipe->notes()) : ?>

                <div class="wprm-notes-container">

                    <h3 class="wprm-recipe-header"><?php echo WPRM_Template_Helper::label('notes'); ?></h3>

                    [wprm-recipe-notes]

                    <div class="add-placeholder">
                    </div>

                </div>

            <?php endif; // Notes ?>

            <div class="wprm-recipe-details-container wprm-recipe-tags-container whole-recipe-prepbox">

                <div class="whole-recipe-prepcook">

                    <?php $taxonomies = WPRM_Taxonomies::get_taxonomies();

                    foreach ($taxonomies as $taxonomy => $options) :

                        $key = substr($taxonomy, 5);
                        $terms = $recipe->tags($key);

                        // Hide keywords from template.
                        if ('keyword' === $key && !WPRM_Settings::get('metadata_keywords_in_template')) {

                            $terms = array();

                        }

                        if (count($terms) > 0) : ?>

                            <div class="wprm-recipe-<?php echo $key; ?>-container wprm-recipe-detail">

                                <strong class="wprm-recipe-details-name wprm-recipe-<?php echo $key; ?>-name"><?php echo WPRM_Template_Helper::label($key . '_tags', $options['singular_name']); ?>
                                    :</strong> <span class="wprm-recipe-<?php echo $key; ?>">

								<?php foreach ($terms as $index => $term) {

                                    if (0 !== $index) {

                                        echo ', ';

                                    }

                                    echo $term->name;

                                } ?>

							</span>

                            </div>

                        <?php endif; // Count.

                    endforeach; // Taxonomies. ?>

                </div>

            </div>

            <?php echo WPRM_Template_Helper::nutrition_label($recipe->id()); ?>

        </div>

        <div class="wprm-entry-footer">

<!--            --><?php
//            $image = get_field('cta_recipe_footer_image');
//            $text = get_field('cta_recipe_footer_text');
//            $link = get_field('cta_recipe_footer_button'); ?>
<!---->
<!--            --><?php //if ($image || $text || $link) { ?>
<!--                <div class="footer-banner">-->
<!--                    --><?php //if ($image) { ?>
<!--                        <div class="footer-banner__image">-->
<!--                            --><?php //echo wp_get_attachment_image($image['ID'], 'medium', false, array('class' => '')); ?>
<!--                        </div>-->
<!--                    --><?php //} ?>
<!--                    --><?php //if ($text || $link) { ?>
<!--                        <div class="footer-banner__text">-->
<!--                            --><?php //if ($text) { ?>
<!--                                --><?php //echo $text; ?>
<!--                            --><?php //} ?>
<!--                            --><?php //if ($link):
//                                $link_url = $link['url'];
//                                $link_title = $link['title'];
//                                $link_target = $link['target'] ? $link['target'] : '_self'; ?>
<!--                                <a class="btn button footer-banner__link"-->
<!--                                   href="--><?php //echo esc_url($link_url); ?><!--"-->
<!--                                   target="--><?php //echo esc_attr($link_target); ?><!--">--><?php //echo esc_html($link_title); ?><!--</a>-->
<!--                            --><?php //endif; ?>
<!--                        </div>-->
<!--                    --><?php //} ?>
<!--                </div>-->
<!--            --><?php //} ?>

            <?php
            $image = get_field('cta_recipe_footer_image', 'options');
            $text = get_field('cta_recipe_footer_text', 'options');
            $link = get_field('cta_recipe_footer_button', 'options'); ?>

            <?php if ($image || $text || $link) { ?>
                <div class="footer-banner">
                    <?php if ($image) { ?>
                        <div class="footer-banner__image">
                            <?php echo wp_get_attachment_image($image['ID'], 'medium', false, array('class' => '')); ?>
                        </div>
                    <?php } else { ?>
                        <div class="footer-banner__image">
                            <img src="https://handletheheat.com/wp-content/uploads/2022/06/HTH-baking-school.webp"
                                 alt="Handle The Heat">
                        </div>
                    <?php } ?>
                    <?php if ($text || $link) { ?>
                        <div class="footer-banner__text">
                            <?php if ($text) { ?>
                                <?php echo $text; ?>
                            <?php } ?>
                            <?php if ($link):
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                                <a class="btn button footer-banner__link"
                                   href="<?php echo esc_url($link_url); ?>"
                                   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>


            <div class="cta-wrap">

                <div class="cta-share">
                    <p><?php _e('SHARE THIS RECIPE', 'handletheheat'); ?></p>
                    <?php echo do_shortcode('[elementor-template id="54922"]'); ?>
                </div>

                <a class="cta-insta" href="https://www.instagram.com/handletheheat/" target="_blank" rel="noopener"><img
                            class="cta-icon pull-left"
                            src="https://www.handletheheat.com/wp-content/uploads/2018/02/instagram-logo-e1605211563551.png"
                            width="48" height="48">
                    <p>Show us!</p></a>
                <div class="cta-text">
                    <p>If you make this recipe, be sure to snap a picture and share it on <a
                                href="https://www.instagram.com/handletheheat/" target="_blank"
                                rel="noopener">Instagram</a>
                        with #handletheheat so we can all see!</p>
                </div>
            </div>
        </div>

    </div>

<?php } else { ?>
    <div class="wprm-recipe wprm-recipe-handle-the-heat wprm-whole-recipe">

        <div class="wprm-entry-header">

            <!-- [wprm-recipe-rating display="stars"] -->
            [wprm-recipe-rating display="stars-details"]

            <h2 class="wprm-recipe-name">
                <div class="how">How to make</div>
                <div class="how_title">[wprm-recipe-name]</div>
            </h2>

            <div class="share"><?php get_template_part('tpl-single-share'); ?></div>

            <div class="wprm-recipe-details-container whole-recipe-prepbox">

                <?php if ($recipe->servings()) : ?>

                    <div class="whole-recipe-yield">

                        <i class="fa fa-cutlery"></i> <strong>Yield:</strong> <span
                                class="wprm-recipe-details wprm-recipe-servings yield"><?php echo $recipe->servings(); ?></span>
                        <span class="wprm-recipe-details-unit wprm-recipe-servings-unit"><?php echo $recipe->servings_unit(); ?></span>

                    </div>

                <?php endif; // Servings. ?>

                <?php if ($recipe->prep_time() || $recipe->cook_time() || ($recipe->custom_time() && $recipe->custom_time_label()) || $recipe->total_time()) : ?>

                    <div class="whole-recipe-prepcook">

                        <?php if ($recipe->prep_time()) : ?>

                            <div class="wprm-recipe-prep-time-container">
                                <i class="fa fa-clock-o"></i> <strong
                                        class="wprm-recipe-details-name wprm-recipe-prep-time-name"><?php echo WPRM_Template_Helper::label('prep_time'); ?></strong>
                                [wprm-recipe-time type="prep"]
                            </div>

                        <?php endif; // Prep time. ?>

                        <?php if ($recipe->cook_time()) : ?>

                            <div class="wprm-recipe-cook-time-container">
                                <i class="fa fa-clock-o"></i> <strong
                                        class="wprm-recipe-details-name wprm-recipe-cook-time-name"><?php echo WPRM_Template_Helper::label('cook_time'); ?></strong>
                                [wprm-recipe-time type="cook"]
                            </div>

                        <?php endif; // Cook time. ?>

                        <?php if ($recipe->custom_time() && $recipe->custom_time_label()) : ?>

                            <div class="wprm-recipe-custom-time-container">
                                <i class="fa fa-clock-o"></i> <strong
                                        class="wprm-recipe-details-name wprm-recipe-custom-time-name"><?php echo $recipe->custom_time_label(); ?></strong>
                                [wprm-recipe-time type="custom"]
                            </div>

                        <?php endif; // Custom time. ?>

                        <?php if ($recipe->total_time()) : ?>

                            <div class="wprm-recipe-total-time-container">
                                <i class="fa fa-clock-o"></i> <strong
                                        class="wprm-recipe-details-name wprm-recipe-total-time-name"><?php echo WPRM_Template_Helper::label('total_time'); ?></strong>
                                [wprm-recipe-time type="total"]
                            </div>

                        <?php endif; // Total time. ?>

                    </div>

                <?php endif; // Times. ?>

            </div>

            <div class="wprm-recipe-image featured">[wprm-recipe-image default="full"]</div>

            <?php if ($recipe->summary() && '<br>' !== $recipe->summary()) { ?>

                <div class="wprm-recipe-summary">[wprm-recipe-summary]</div>

            <?php } ?>

        </div>

        <div class="wprm-entry-content">

            <?php $ingredients = $recipe->ingredients();
            if (count($ingredients) > 0) : ?>

                <div class="wprm-recipe-ingredients-container ingredients">

                    <h3 class="wprm-recipe-header title"><?php echo WPRM_Template_Helper::label('ingredients'); ?></h3>

                    <?php foreach ($ingredients as $ingredient_group) : ?>

                        <div class="wprm-recipe-ingredient-group">

                            <?php if ($ingredient_group['name']) : ?>

                                <h4 class="wprm-recipe-group-name wprm-recipe-ingredient-group-name"><?php echo $ingredient_group['name']; ?></h4>

                            <?php endif; // Ingredient group name. ?>

                            <ul class="wprm-recipe-ingredients ingredient">

                                <?php foreach ($ingredient_group['ingredients'] as $ingredient) : ?>

                                    <li class="wprm-recipe-ingredient">

                                        <?php if ($ingredient['amount']) : ?>

                                            <span class="wprm-recipe-ingredient-amount"><?php echo $ingredient['amount']; ?></span>

                                        <?php endif; // Ingredient amount. ?>

                                        <?php if ($ingredient['unit']) : ?>

                                            <span class="wprm-recipe-ingredient-unit"><?php echo $ingredient['unit']; ?></span>

                                        <?php endif; // Ingredient unit. ?>

                                        <span class="wprm-recipe-ingredient-name"><?php echo WPRM_Template_Helper::ingredient_name($ingredient, true); ?></span>

                                        <?php if ($ingredient['notes']) : ?>

                                            <span class="wprm-recipe-ingredient-notes"><?php echo $ingredient['notes']; ?></span>

                                        <?php endif; // Ingredient notes. ?>

                                    </li>

                                <?php endforeach; // Ingredients. ?>

                            </ul>

                        </div>

                    <?php endforeach; // Ingredient groups. ?>

                    <?php echo WPRM_Template_Helper::unit_conversion($recipe); ?>

                </div>

            <?php endif; // Ingredients. ?>

            <?php $instructions = $recipe->instructions();
            if (count($instructions) > 0) : ?>

                <div class="wprm-recipe-instructions-container directions">

                    <h3 class="wprm-recipe-header title"><?php echo WPRM_Template_Helper::label('instructions'); ?></h3>

                    <?php foreach ($instructions as $instruction_group) : ?>

                        <div class="wprm-recipe-instruction-group">

                            <?php if ($instruction_group['name']) : ?>

                                <h4 class="wprm-recipe-group-name wprm-recipe-instruction-group-name"><?php echo $instruction_group['name']; ?></h4>

                            <?php endif; // Instruction group name. ?>

                            <ol class="wprm-recipe-instructions instructions">

                                <?php foreach ($instruction_group['instructions'] as $instruction) : ?>

                                    <li class="wprm-recipe-instruction">

                                        <?php if ($instruction['text']) : ?>

                                            <div class="wprm-recipe-instruction-text"><?php echo $instruction['text']; ?></div>

                                        <?php endif; // Instruction text. ?>

                                        <?php if ($instruction['image']) : ?>

                                            <div class="wprm-recipe-instruction-image"><?php echo WPRM_Template_Helper::instruction_image($instruction, 'thumbnail'); ?></div>

                                        <?php endif; // Instruction image. ?>

                                    </li>

                                <?php endforeach; // Instructions. ?>

                            </ol>

                        </div>

                    <?php endforeach; // Instruction groups. ?>

                </div>

            <?php endif; // Instructions. ?>

            <?php if ($recipe->video()) : ?>

                <div class="wprm-recipe-video-container">

                    <h3 class="wprm-recipe-header"><?php echo WPRM_Template_Helper::label('video'); ?></h3>

                    [wprm-recipe-video]

                </div>

            <?php endif; // Video. ?>

            <?php if ($recipe->notes()) : ?>

                <div class="wprm-recipe-notes-container">

                    <h3 class="wprm-recipe-header"><?php echo WPRM_Template_Helper::label('notes'); ?></h3>

                    [wprm-recipe-notes]

                </div>

            <?php endif; // Notes ?>

            <div class="wprm-recipe-details-container wprm-recipe-tags-container whole-recipe-prepbox">

                <div class="whole-recipe-prepcook">

                    <?php $taxonomies = WPRM_Taxonomies::get_taxonomies();

                    foreach ($taxonomies as $taxonomy => $options) :

                        $key = substr($taxonomy, 5);
                        $terms = $recipe->tags($key);

                        // Hide keywords from template.
                        if ('keyword' === $key && !WPRM_Settings::get('metadata_keywords_in_template')) {

                            $terms = array();

                        }

                        if (count($terms) > 0) : ?>

                            <div class="wprm-recipe-<?php echo $key; ?>-container wprm-recipe-detail">

                                <strong class="wprm-recipe-details-name wprm-recipe-<?php echo $key; ?>-name"><?php echo WPRM_Template_Helper::label($key . '_tags', $options['singular_name']); ?>
                                    :</strong> <span class="wprm-recipe-<?php echo $key; ?>">

								<?php foreach ($terms as $index => $term) {

                                    if (0 !== $index) {

                                        echo ', ';

                                    }

                                    echo $term->name;

                                } ?>

							</span>

                            </div>

                        <?php endif; // Count.

                    endforeach; // Taxonomies. ?>

                </div>

            </div>

            <?php echo WPRM_Template_Helper::nutrition_label($recipe->id()); ?>

        </div>

        <div class="wprm-entry-footer">

            <?php
            $image = get_field('cta_recipe_footer_image', 'options');
            $text = get_field('cta_recipe_footer_text', 'options');
            $link = get_field('cta_recipe_footer_button', 'options'); ?>

            <?php if ($image || $text || $link) { ?>
                <div class="footer-banner">
                    <?php if ($image) { ?>
                        <div class="footer-banner__image">
                            <?php echo wp_get_attachment_image($image['ID'], 'medium', false, array('class' => '')); ?>
                        </div>
                    <?php } else { ?>
                        <div class="footer-banner__image">
                            <img src="https://handletheheat.com/wp-content/uploads/2022/06/HTH-baking-school.webp"
                                 alt="Handle The Heat">
                        </div>
                    <?php } ?>
                    <?php if ($text || $link) { ?>
                        <div class="footer-banner__text">
                            <?php if ($text) { ?>
                                <?php echo $text; ?>
                            <?php } ?>
                            <?php if ($link):
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self'; ?>
                                <a class="btn button footer-banner__link"
                                   href="<?php echo esc_url($link_url); ?>"
                                   target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                            <?php endif; ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <!--	    <div>-->
            <!--			<a href="https://app.monstercampaigns.com/c/qvbgymzu99rp7nkid4co/" target="_blank"><img src="https://lh3.googleusercontent.com/EaTZ4TpcXtU86FjeSbgw0CEJttzEZP2g8I2NqYO7e25AF5pxjX9cKdaC6jMNykn9YoZ6KL-CcM-tdzVv8Kie=s0" width="637" height="255"></a>-->
            <!--		</div>-->
            <!---->
            <!--		<div class="cta-wrap">-->
            <!---->
            <!--			<img class="cta-icon pull-left" src="https://www.handletheheat.com/wp-content/uploads/2018/02/instagram-logo-e1605211563551.png" width="48" height="48">-->
            <!---->
            <!--			<span class="cta-text">If you make this recipe, be sure to snap a picture and share it on <a href="https://www.instagram.com/handletheheat/" target="_blank" rel="noopener">Instagram</a> with #handletheheat so we can all see!</span>-->
            <!---->
            <!--		</div>-->

            <?php if ($recipe->author()) : ?>

                <div class="wprm-recipe-author-container recipeby">
                    <span class="wprm-recipe-details-name wprm-recipe-author-name"><?php echo WPRM_Template_Helper::label('author'); ?></span>
                    <span class="wprm-recipe-details wprm-recipe-author">[wprm-recipe-author]</span>
                </div>

            <?php endif; // Author. ?>


        </div>

    </div>
<?php } ?>
