<?php if ( !function_exists('dynamic_sidebar')
		  || !dynamic_sidebar('Ideal Sidebar') ) : ?>
<?php endif; ?>
<div class="recipe_search">
	<h3 class="searchtitle"><i class="fa fa-tags"></i>Recipe Search</h3>
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		<input type="search" id="s" name="s" placeholder="Find a recipe...">
		<i class="fa fa-search"></i>					        
		<input type="submit" value="" id="searchsubmit" />
	</form>
	<?php echo do_shortcode('[searchandfilter id="8686"]'); ?>
</div>