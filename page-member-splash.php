<?php 
    // Template Name: Members Splash
?>

<?php 

// Check for user to be logged into the WP Admin

if(is_user_logged_in()) {
	$user = wp_get_current_user();
	// Is the user a customer?

	if ( in_array( 'customer', (array) $user->roles ) || in_array( 'subscriber', (array) $user->roles ) ) {
	    $logged_in_proper = true;
	}

	// Is the user an administrator

	else if ( in_array( 'administrator', (array) $user->roles ) ) {
	    $logged_in_proper = true;
	}

	// The user is NOT what we're looking for // redirect them

	else {
		$logged_in_proper = false;
		header('Location: https://handletheheat.com');
		exit;
	}
}

// User is NOT logged in

else {
	$logged_in_proper = false;
	header('Location: https://handletheheat.com');
	exit;
}

?>

<?php

// Check the above to see if we're supposed to show the WP Content or NOT - if YES - you can use Elementor
// If not - they will get nothing here - just a blank if they mananged to escape the top

if($logged_in_proper === true) { ?>

<?php get_header(); ?>

    <div class="members-splash" id="members-splash">
        
        <div class="members-splash-container">
                    
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php the_content(); ?>

			<?php endwhile; endif; ?>

		</div>

	</div>


<?php get_footer(); ?>

<?php } ?>