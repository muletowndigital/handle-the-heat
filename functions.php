<?php
// Added by Shakey G Applications

function hth_custom_fonts()
{
    wp_enqueue_style('hth-google-fonts-raleway', 'https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap', false);
    wp_enqueue_style('hth-google-fonts-lora', 'https://fonts.googleapis.com/css2?family=Lora:ital,wght@0,400;0,700;1,400;1,700&display=swap', false);
    wp_enqueue_style('hth-google-fonts-lato', 'https://fonts.googleapis.com/css2?family=Lato:wght@300;700;900&display=swap', false);
    wp_enqueue_style('hth-google-fonts-abril-fatface', 'https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap', false);
    wp_enqueue_style('hth-font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false);
    wp_enqueue_style('hth-webtype', '//cloud.webtype.com/css/bc33bd47-2f28-4a3f-bdbc-24623d6732c7.css', false);
    wp_enqueue_style('hth-custom-fonts', get_template_directory_uri() . '/fonts/fonts-stylesheet.css', false);
}

add_action('wp_enqueue_scripts', 'hth_custom_fonts');

function hth_styles()
{
    wp_enqueue_style('hth-main-style', get_template_directory_uri() . '/style.css', array(), null, false);
    wp_enqueue_style('hth-main-styles', get_template_directory_uri() . '/styles.css', array(), null, false);
    wp_enqueue_style('hth-lightslider', get_template_directory_uri() . '/css/lightslider.css', array(), null, false);
    wp_enqueue_style('hth-menu', get_template_directory_uri() . '/css/menu.css', array(), null, false);
    wp_enqueue_style('hth-mtd-custom', get_template_directory_uri() . '/css/mtd-challenge-page.css', array(), null, false);
}

add_action('wp_enqueue_scripts', 'hth_styles');

function hth_scripts()
{
    wp_enqueue_script('hth-formalize', get_template_directory_uri() . '/js/jquery.formalize.js', array('jquery'), null, false);
    wp_enqueue_script('hth-mixitup', get_template_directory_uri() . '/js/jquery.mixitup.min.js', array('jquery'), null, false);
    wp_enqueue_script('hth-responsiveTabs', get_template_directory_uri() . '/js/jquery.responsiveTabs.min.js', array('jquery'), null, false);
    wp_enqueue_script('hth-colorbox', get_template_directory_uri() . '/js/jquery.colorbox-min.js', array('jquery'), null, false);
    wp_enqueue_script('hth-lightslider', get_template_directory_uri() . '/js/lightslider.js', array('jquery'), null, false);
    wp_enqueue_script('hth-menu', get_template_directory_uri() . '/js/menu.js', array('jquery'), null, false);
    wp_enqueue_script('hth-comment-section', get_template_directory_uri() . '/js/comment-section-scripts.js', array('jquery'), 15, true);
    wp_enqueue_script('hth-jQuery-custom', get_template_directory_uri() . '/js/jQueryCustom.js', array('jquery'), null, false);
    wp_enqueue_script('hth-pinit', 'https://assets.pinterest.com/js/pinit.js', array('jquery'), null, false);

}

add_action('wp_enqueue_scripts', 'hth_scripts');

function gleam_scripts()
{
    if (has_tag('challenge')) {
        wp_enqueue_script('hth-gleam', 'https://cdn.galleryjs.io/webpack/gallery-v2.launcher.js', array(), null, false);
    }
}

add_action('wp_enqueue_scripts', 'gleam_scripts');

/***********************************************************************************/
/* ACF Options Panel Setup */
/***********************************************************************************/

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'  => 'Express Settings',
        'menu_title'  => 'Express Settings',
        'menu_slug'   => 'mtd-express-settings',
        'capability'  => 'edit_posts',
        'redirect'    => false,
        'position'    => 2,
    ));
}

function hth_add_disable_adthrive_body_class_for_members($classes)
{
    if (function_exists('wc_memberships')) {
        $user_id = get_current_user_id();
        //Get User Role
        $user_role = wp_get_current_user();
        $args = array(
            'status' => array('cancelled', 'complimentary', 'pending'),
        );
        $active_memberships = wc_memberships_get_user_memberships($user_id, $args);

        //Turn off ads for all subscribers
        if (in_array('subscriber', (array)$user_role->roles)) {
            $classes[] = 'adthrive-disable-all';
        }
        if (!empty($active_memberships)) {
            $classes[] = 'adthrive-disable-all';
        }
    }

    return $classes;
}

add_filter('body_class', 'hth_add_disable_adthrive_body_class_for_members');

function hth_add_disable_adthrive_body_class_for_flow_steps($classes)
{
    if (get_post_type(get_the_ID()) == 'cartflows_step') {
        $classes[] = 'adthrive-disable-all';
    }

    return $classes;
}

add_filter('body_class', 'hth_add_disable_adthrive_body_class_for_flow_steps');

function hth_disable_adthrive_for_all_customers($classes)
{
    /*if ( current_user_can( 'customer' ) ) {
        $classes[] = 'adthrive-disable-all';
    }*/

    $ads_user = wp_get_current_user();

    if ( in_array( 'customer', (array) $ads_user->roles ) ) {
        $classes[] = 'adthrive-disable-all adthrive-current-customer';
    }

    return $classes;
}

add_filter('body_class', 'hth_disable_adthrive_for_all_customers');

function helpwise_scripts()
{
    if (function_exists('wc_memberships')) {
        $user_id = get_current_user_id();
        $args = array(
            'status' => array('active', 'complimentary', 'pending'),
        );
        $active_memberships = wc_memberships_get_user_memberships($user_id, $args);

        if (!empty($active_memberships)) {
            wp_enqueue_script('hth-helpswise', 'https://cdn.helpwise.io/assets/js/livechat.js', array(), null, false);
            echo "<script>helpwiseSettings = {widget_id: '6047ff097066c',align:'right',}</script>";
        }
    }
}

add_action('wp_enqueue_scripts', 'helpwise_scripts');

function hth_add_woocommerce_support()
{
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'hth_add_woocommerce_support');

add_filter('woocommerce_defer_transactional_emails', '__return_true');

function hth_remove_inactive_members_activecampaign($user_membership, $old_status, $new_status)
{
    // this runs when a members status changes. i.e. Active -> Cancelled/Expired etc.

    // get the activecampaign creds from the activecampign plugin
    $ac_creds = get_option('settings_activecampaign');

    // get member info
    $user_id = $user_membership->get_user_id();
    $member = get_userdata($user_id);

    // bail if member is active or any of the credentials do not exist
    // this is useful if status is going from Cancelled -> Active
    if (wc_memberships_is_user_active_member($user_id, $user_membership->get_plan_id()) || !$ac_creds['api_key'] || !$ac_creds['api_url']) {
        return;
    }

    // query ac for member email
    // we do this to get the contacts id in ac. We need this to find the contactTag id in order to delete it
    $contact_url = $ac_creds['api_url'] . '/api/3/contacts?filters[email]=' . $member->user_email;

    $request_args = array(
        'headers' => array(
            'Api-Token' => $ac_creds['api_key']
        )
    );

    $contact_request = wp_remote_get($contact_url, $request_args);

    if (is_wp_error($contact_request)) {
        return;
    }

    $contact_response = wp_remote_retrieve_body($contact_request);

    $matched_contacts = json_decode($contact_response, true);

    // filter the contacts for an exact match in case there are several matches for the email
    $contact = array_filter($matched_contacts["contacts"], function ($value) use ($member) {
        return $value["email"] = $member->user_email;
    });

    // if emails no emails found, bail
    if (empty($contact)) {
        return;
    }

    $contact_id = $contact[0]['id'];

    // get tag from acf options
    $cancellation_tag_wp = get_field('membership_cancellation_tag', 'option');
    $cancellation_tag_decscription_wp = get_field('membership_cancellation_tag_description', 'option');

    // get all mtached tags
    $tags_url = $ac_creds['api_url'] . '/api/3/tags?filters[tag]=' . $cancellation_tag_wp;
    $tags_request = wp_remote_get($tags_url, $request_args);

    if (is_wp_error($tags_request)) {
        return;
    }

    $tags_response = wp_remote_retrieve_body($tags_request);

    $tags = json_decode($tags_response, true);

    // find id of tag from acf options
    $cancellation_tag_ac = array_filter($tags['tags'], function ($value) use ($cancellation_tag_wp) {
        return $value['tag'] == $cancellation_tag_wp;
    });

    // if tag doesnt exist, create it
    if (empty($cancellation_tag_ac)) {
        // create tag
        $create_tag_body = [
            'tag' => [
                'tag' => $cancellation_tag_wp,
                'tagType' => 'contact',
                'description' => $cancellation_tag_decscription_wp
            ]
        ];

        $create_tag_body_json = wp_json_encode($create_tag_body);

        $create_tag_url = $ac_creds['api_url'] . '/api/3/tags';

        $create_tag_response = wp_remote_post($create_tag_url, array(
            'body' => $create_tag_body_json,
            'headers' => $request_args['headers']
        ));

        $create_tag = json_decode($create_tag_response, true);
    }

    // if cancellation_tag_ac is empty use the created tag. Otherwise use the tag id from ac
    $cancellation_tag_id = empty($cancellation_tag_ac) ? $create_tag['tag']['id'] : $cancellation_tag_ac[0]['id'];

    // add tag id to contact id
    $add_tag_body = [
        'contactTag' => [
            'contact' => $contact_id,
            'tag' => $cancellation_tag_id
        ]
    ];

    $add_tag_body_json = wp_json_encode($add_tag_body);

    $add_tag_url = $ac_creds['api_url'] . '/api/3/contactTags';

    $add_tag_response = wp_remote_post($add_tag_url, array(
        'body' => $add_tag_body_json,
        'headers' => $request_args['headers']
    ));

    $add_tag = json_decode($add_tag_response, true);
}

add_action('wc_memberships_user_membership_status_changed', 'hth_remove_inactive_members_activecampaign', 10, 3);


//function hth_login_logout_link($items, $args)
//{
//    if ($args->theme_location == 'main' || $args->theme_location == 'footer-menu-two') {
//        $loginoutlink = wp_loginout('', false);
//        $items .= '<li class="menu-item">' . $loginoutlink . '</li>';
//        return $items;
//    }
//    return $items;
//}
//
//add_filter('wp_nav_menu_items', 'hth_login_logout_link', 10, 2);

// Send failed order email to customer
// This is email is WooCommerce > Settings > Emails > Failed Order
function hth_send_failed_order_email_notification($order_id, $old_status, $new_status, $order)
{
    if ($new_status == 'failed') {
        $wc_emails = WC()->mailer()->get_emails(); // Get all WC_emails objects instances
        $customer_email = $order->get_billing_email(); // The customer email

        // change the recipient of this instance
        $wc_emails['WC_Email_Failed_Order']->recipient .= ',' . $customer_email;

        // Sending the email from this instance
        $wc_emails['WC_Email_Failed_Order']->trigger($order_id);
    }
}

add_action('woocommerce_order_status_changed', 'hth_send_failed_order_email_notification', 10, 4);

// End Shakey G Applications functions

// Register Menus
add_theme_support('menus');
function register_theme_menu()
{
    register_nav_menus(
        array(
            'main' => __('Main Menu'),
            'mobile-menu' => __('Mobile Menu'),
            'footer-menu' => __('Footer Menu'),
            'footer-menu-one' => __('Footer Menu 1'),
            'footer-menu-two' => __('Footer Menu 2'),
            'footer-menu-three' => __('Footer Menu 3'),
        )
    );

}


add_action('init', 'register_theme_menu');

// Current Date
function current_date($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return date('Y');
}
add_shortcode('current_date', 'current_date');

// Featured Images
add_theme_support('post-thumbnails');
add_image_size('teaser', 230, 230, true);
add_image_size('filter-recipe', 270, 270, true);
add_image_size('recipe_single', 550, 550, true);


// http://support.advancedcustomfields.com/forums/topic/no-options-pages-exist-error-on-acf-pro-5-0-4/
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Recipe Picks',
        'menu_title' => 'Recipe Picks',
        'menu_slug' => 'recipe_picks',
        'capability' => 'edit_pages',
        'redirect' => false
    ));

    acf_add_options_page(array(
        'page_title' => 'ActiveCampaign Tags',
        'menu_title' => 'ActiveCampaign Tags',
        'menu_slug' => 'activecampaign_tags',
        'capability' => 'edit_pages',
        'icon_url' => 'dashicons-tag',
    ));

}


// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
    add_filter('show_admin_bar', '__return_false');
}


// GF - enable field label visibility
add_filter('gform_enable_field_label_visibility_settings', '__return_true');


/**
 * Gravity Forms Custom Activation Template
 * http://gravitywiz.com/customizing-gravity-forms-user-registration-activation-page
 */
add_action('wp', 'custom_maybe_activate_user', 9);
function custom_maybe_activate_user()
{

    $template_path = STYLESHEETPATH . '/activate/activate.php';
    $is_activate_page = isset($_GET['page']) && $_GET['page'] == 'gf_activation';

    if (!file_exists($template_path) || !$is_activate_page)
        return;

    require_once($template_path);

    exit();
}


// Redirect back to homepage and not allow access to WP admin for Subscribers.
function hth_redirect_admin()
{
    if (!defined('DOING_AJAX') && !current_user_can('edit_posts')) {
        wp_redirect(site_url());
        exit;
    }
}

add_action('admin_init', 'hth_redirect_admin');


// redirect login page
function redirect_login_page()
{
    $login_page = home_url('/login/');
    $page_viewed = basename($_SERVER['REQUEST_URI']);

    if ($page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
        wp_redirect($login_page);
        exit;
    }
}

add_action('init', 'redirect_login_page');

// Sidebars

if (function_exists('register_sidebar'))
    register_sidebar(array('name' => 'Top Sidebar',
        'id' => 'top-sidebar',
        'description' => 'Top widget section of sidebar. Space for 300x250px banner ad',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>',
    ));
register_sidebar(array('name' => 'Top Sidebar 2',
    'id' => 'top-sidebar-2',
    'description' => 'Most Popular Recipes',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));
register_sidebar(array('name' => 'Left Sidebar',
    'id' => 'left-sidebar',
    'description' => 'Sidebar left column, maximum 125px wide elements.',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));
register_sidebar(array('name' => 'Right Sidebar',
    'id' => 'right-sidebar',
    'description' => 'Sidebar right column. Ad space for a 160x600 skyscraper banner ad. Insert a text widget with the ad code to use. No ad? Leave this blank, or fill it with other widgets as you see fit!',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));
register_sidebar(array('name' => 'Lower Sidebar',
    'id' => 'lower-sidebar',
    'description' => 'Lower widget section of sidebar. Space for 300x250px banner ads.',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));
register_sidebar(array('name' => 'Lower Sidebar 2',
    'id' => 'lower-sidebar-2',
    'description' => 'Lower widget section of sidebar. Space for 300x250px banner ads.',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));
register_sidebar(array('name' => 'Below Post Adspace',
    'id' => 'below-post-adspace',
    'description' => 'Ad space below the post content but above the comments.',
    'before_widget' => '<div id="%1$s" class="adspace %2$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));
// register_sidebar(array('name'=>'Footer',
// 'id' => 'footer-widget',
// 'description' => 'Ad space for a 468x60 or 728x90 banner ad to display in footer. Insert a text widget with the ad code to use. No ad? Leave this blank.',
// 'before_widget' => '<div id="%1$s" class="footerad adspace %2$s">',
// 'after_widget' => '</div>',
// 'before_title' => '<h3 class="widgettitle">',
// 'after_title' => '</h3>',
// ));
// register_sidebar(array('name'=>'Tabbed Widget 1',
// 'id' => 'tabbed-widget-1',
// 'description' => 'Tabbed Widget Area',
// 'before_widget' => '<div class="tabwidget %2$s">',
// 'after_widget' => '</div>',
// 'before_title' => '<h2>',
// 'after_title' => '</h2>',
// ));
register_sidebar(array('name' => 'Ideal Sidebar',
    'id' => 'ideal-sidebar',
    'description' => 'The Sidebar for future versions of Handle the Heat.',
    'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widgettitle">',
    'after_title' => '</h3>',
));
// 	register_sidebar(array('name'=>'Instagram Footer',
// 'id' => 'instagram-footer',
// 'description' => 'Display Instagram Feed',
// 'before_widget' => '<div id="%1$s" class="%2$s">',
// 'after_widget' => '</div>',
// 'before_title' => '<h3 class="widgettitle">',
// 'after_title' => '</h3>',
// ));
// register_sidebar(array('name'=>'Touch Ad',
// 'id' => 'touch-ad',
// 'description' => 'Ad for touch/tablet/mobile screens.',
// 'before_widget' => '<li id="%1$s" class="widget %2$s">',
// 'after_widget' => '</li>',
// 'before_title' => '<h3 class="widgettitle">',
// 'after_title' => '</h3>',
// ));

// Register Blog post type

function blog_post_type()
{

    // Labels
    $labels = array(
        'name' => _x("Blog", "post type general name"),
        'singular_name' => _x("Blog", "post type singular name"),
        'menu_name' => 'Blog',
        'add_new' => _x("Add New", "header item"),
        'add_new_item' => __("Add New Blog Post"),
        'edit_item' => __("Edit Blog Post"),
        'new_item' => __("New Blog Post"),
        'view_item' => __("View Blog Post"),
        'search_items' => __("Search Blog Posts"),
        'not_found' => __("No Blog Posts Found"),
        'not_found_in_trash' => __("No Blog Posts Found in Trash"),
        'parent_item_colon' => ''
    );

    // Register post type
    register_post_type('blog', array(
        'labels' => $labels,
        'menu_position' => 5,
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'blogs', 'with_front' => true),
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'revisions')
        // 'taxonomies' => array('category', 'post_tag')
    ));
}

add_action('init', 'blog_post_type', 0);


// hook into the init action and call create_book_taxonomies when it fires
add_action('init', 'create_categories_hierarchical_taxonomy', 0);

// create a custom taxonomy name it topics for your posts

function create_categories_hierarchical_taxonomy()
{

// Add new taxonomy, make it hierarchical like categories
// first do the translations part for GUI

    $labels = array(
        'name' => _x('Category', 'taxonomy general name'),
        'singular_name' => _x('Category', 'taxonomy singular name'),
        'search_items' => __('Search Categories'),
        'all_items' => __('All Categories'),
        'parent_item' => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item' => __('Edit Category'),
        'update_item' => __('Update Category'),
        'add_new_item' => __('Add New Category'),
        'new_item_name' => __('New Category Name'),
        'menu_name' => __('Categories'),
    );

// Now register the taxonomy

    register_taxonomy('categories', array('blog'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'type'),
    ));

}


/**
 * Remove the slug from custom post type permalinks.
 */
function nsppt_remove_cpt_slug($post_link, $post, $leavename)
{
    if (!in_array($post->post_type, array('blog')) || 'publish' != $post->post_status)
        return $post_link;
    $post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);
    return $post_link;
}

add_filter('post_type_link', 'nsppt_remove_cpt_slug', 10, 3);
/**
 * Some hackery to have WordPress match postname to any of our public post types
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * Typically core only accounts for posts and pages where the slug is /post-name/
 */
function nsppt_parse_request_tricksy($query)
{
    // Only noop the main query
    if (!$query->is_main_query())
        return;
    // Only noop our very specific rewrite rule match
    if (2 != count($query->query)
        || !isset($query->query['page']))
        return;
    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
    if (!empty($query->query['name']))
        $query->set('post_type', array('post', 'blog', 'page'));
}

add_action('pre_get_posts', 'nsppt_parse_request_tricksy');


if (!function_exists('custom_comment')) :
    /**
     * Template for comments and pingbacks.
     **/
    function custom_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case 'comment' :
                ?>
                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                <div id="comment-<?php comment_ID(); ?>">
                    <?php echo get_avatar($comment, 48); ?>
                    <a class="commentnumber"
                       href="#comment-<?php comment_ID() ?>">#<?php if (function_exists('gtcn_comment_numbering')) echo gtcn_comment_numbering($comment->comment_ID, $args); ?></a>
                    <div class="commentmeta"><strong><?php comment_author() ?></strong>
                        &#8212; <?php comment_date('F j, Y') ?>
                        <em>at</em> <?php comment_time() ?> <?php edit_comment_link(__("Edit")); ?></div>

                    <div class="comment-body"><?php comment_text(); ?></div>

                    <?php
                    comment_reply_link(array_merge($args, array(
                        // 			'add_below' => $add_below,
                        'depth' => $depth,
                        'max_depth' => $args['max_depth'],
                        'before' => '<div class="reply">',
                        'after' => '</div>'
                    )));

                    ?>

                </div><!-- #comment-##  -->

                <?php
                break;
            case 'pingback'  :
            case 'trackback' :
                ?>
                <li <?php comment_class(); ?>>
                <p><?php _e('Pingback:', 'twentyten'); ?><?php comment_author_link(); ?></p>
                <?php
                break;
        endswitch;
    }
endif;


// re-use the old [cft] shortcode to output the new recipe info - see single-launch.php
function cft_func()
{
    global $content;
    ob_start();
    get_template_part('tpl', 'old-recipe');
    $output = ob_get_clean();
    return $output;
}

add_shortcode('cft', 'cft_func');


// https://css-tricks.com/snippets/wordpress/get-the-first-image-from-a-post/

function catch_that_image()
{
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches[1][0];

    if (empty($first_img)) {
        $first_img = get_template_directory_uri() . '/images/fallback.png';
    }
    return $first_img;
}


// change the class output for avatars so that they dont conflict with the hrecipe photo class we need for displaying pics of the food
add_filter('get_avatar', 'change_avatar_css');
function change_avatar_css($class)
{
    $class = str_replace("photo", "updatedphoto", $class);
    return $class;
}


// http://www.wpbeginner.com/wp-tutorials/how-to-display-authors-twitter-and-facebook-on-the-profile-page/
function my_new_contactmethods($contactmethods)
{
    $contactmethods['pinterest'] = 'Pinterest profile URL';
    $contactmethods['youtube'] = 'YouTube profile URL';
    $contactmethods['instagram'] = 'Instagram profile URL';

    return $contactmethods;
}

add_filter('user_contactmethods', 'my_new_contactmethods', 10, 1);


add_filter('the_content', function ($content) {
    $recipe = get_query_var('recipe');
    if (!function_exists('get_field') || $recipe) return $content;
    if ((get_field('recipe_yield')) || (get_field('recipe_prep')) || (get_field('recipe_time')) || (get_field('recipe_ingredients')) || (get_field('recipe_directions'))) {
        if (strpos($content, '[cft') === false) {
            $content .= '[cft format="0"]';
        }
    }
    return $content;
});

function override_mce_options($initArray)
{
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}

add_filter('tiny_mce_before_init', 'override_mce_options');

/*
 * Save value of comment checkboxes when comment is submitted
 */
add_action('comment_post', 'save_comment_checkboxes');
function save_comment_checkboxes($post_id)
{

    $question = $_POST['comment-option-question'];
    $review = $_POST['comment-option-review'];

    add_comment_meta($post_id, 'comment-option-question', $question, true);
    add_comment_meta($post_id, 'comment-option-review', $review, true);

}


/*
 * Add class on comments to indicate which type of comment it was
 */
add_filter('comment_class', 'add_checkbox_value_classes_to_comments', 10, 5);
function add_checkbox_value_classes_to_comments($classes, $class, $comment_ID, $comment, $post_id)
{

    if (get_comment_meta($comment_ID, 'comment-option-question', true)) {
        $classes[] = "option-question";
    }

    if (get_comment_meta($comment_ID, 'comment-option-review', true)) {
        $classes[] = "option-review";
    }

    return $classes;

}

// Add an edit option to comment editing screen
add_action('add_meta_boxes_comment', 'extend_comment_add_meta_box');
function extend_comment_add_meta_box()
{
    add_meta_box('title', __('Comment Type'), 'extend_comment_meta_box', 'comment', 'normal', 'high');
}

function extend_comment_meta_box($comment)
{

    $commentId = get_comment_ID();
    $question = get_comment_meta($commentId, 'comment-option-question', true);
    $review = get_comment_meta($commentId, 'comment-option-review', true);

    wp_nonce_field('extend_comment_update', 'extend_comment_update', false);
    ?>

    <p class="comment-form-options">

        <?php if ($question) {
            ?><span><input id="comment-option-question" name="comment-option-question" type="checkbox" checked/><label
                        for="comment-option-question">I have a question</label></span><?php
        } else {
            ?><span><input id="comment-option-question" name="comment-option-question" type="checkbox"/><label
                        for="comment-option-question">I have a question</label></span><?php
        } ?>

        <?php if ($review) {
            ?><span><input id="comment-option-review" name="comment-option-review" type="checkbox" checked/><label
                        for="comment-option-review">I made this</label></span><?php
        } else {
            ?><span><input id="comment-option-review" name="comment-option-review" type="checkbox"/><label
                        for="comment-option-review">I made this</label></span><?php
        } ?>

    </p>
    <?php
}

// Update comment meta data from comment editing screen
add_action('edit_comment', 'extend_comment_edit_metafields');
function extend_comment_edit_metafields($comment_id)
{

    if (!isset($_POST['extend_comment_update']) || !wp_verify_nonce($_POST['extend_comment_update'], 'extend_comment_update')) return;

    if ((isset($_POST['comment-option-question'])) && ($_POST['comment-option-question'] != '')):
        $question = wp_filter_nohtml_kses($_POST['comment-option-question']);
        update_comment_meta($comment_id, 'comment-option-question', $question);
    else :
        delete_comment_meta($comment_id, 'comment-option-question');
    endif;

    if ((isset($_POST['comment-option-review'])) && ($_POST['comment-option-review'] != '')):
        $review = wp_filter_nohtml_kses($_POST['comment-option-review']);
        update_comment_meta($comment_id, 'comment-option-review', $review);
    else :
        delete_comment_meta($comment_id, 'comment-option-review');
    endif;

}


function theme_queue_js()
{
    if ((!is_admin()) && is_singular() && comments_open() && get_option('thread_comments'))
        wp_enqueue_script('comment-reply');
}

add_action('wp_head', 'theme_queue_js');

add_filter('avatar_defaults', 'add_default_gravatar');
function add_default_gravatar($avatar_defaults)
{

    $myavatar = get_template_directory_uri() . '/images/HTH_gravatar.png';
    $avatar_defaults[$myavatar] = "Default Gravatar";
    return $avatar_defaults;

}
function comment_styles()
{
    global $wp_styles;
    wp_enqueue_style('mtd-comments-css', get_template_directory_uri() . '/css/mtd-comments.css', array(), '', 'all');
}

add_action('wp_enqueue_scripts', 'comment_styles', 99);

function recipe_grid_styles()
{
    global $wp_styles;
    wp_enqueue_style('mtd-recipe-css', get_template_directory_uri() . '/css/mtd-recipes-page.css', array(), '', 'all');
}

add_action('wp_enqueue_scripts', 'recipe_grid_styles', 99);

function mtd_comments_scripts()
{
    wp_enqueue_script('mtd-comment-script', get_template_directory_uri() . '/js/mtd-comments.js', array('jquery'), null, false);
}

add_action('wp_enqueue_scripts', 'mtd_comments_scripts');

function mtd_category_styles()
{
    global $wp_styles;
    wp_enqueue_style('mtd-category-page', get_template_directory_uri() . '/css/mtd-category-page.css', array(), '', 'all');
}

add_action('wp_enqueue_scripts', 'mtd_category_styles', 99);

// Adds Star Rating to specific Posts

function be_star_rating()
{

    // Return early if WP Recipe Maker isn't active
    if (!class_exists('WPRM_Recipe_Manager'))
        return;

    global $post;
    $recipes = WPRM_Recipe_Manager::get_recipe_ids_from_content($post->post_content);
    if (isset($recipes[0])) {
        $recipe_id = $recipes[0];
        return '<div class="custom-recipe-ratings">' . do_shortcode('[wprm-recipe-rating id="' . $recipe_id . '"]') . '</div>';
    }

}
//Add WP Support Title
add_theme_support( 'title-tag' );

// Posts Grid Shortcode For Recipes Page
function recipes_grid() {

    ?>
    <div class="content">


            <?php get_template_part('tpl-searchgrid'); ?>

    <?php

}
add_shortcode('recipes_grid', 'recipes_grid');

//Make WP Search Titles Only
function __search_by_title_only( $search, $wp_query )
{
    global $wpdb;
    if(empty($search)) {
        return $search; // skip processing - no search term in query
    }
    $q = $wp_query->query_vars;
    $n = !empty($q['exact']) ? '' : '%';
    $search =
    $searchand = '';
    foreach ((array)$q['search_terms'] as $term) {
        $term = esc_sql($wpdb->esc_like($term));
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }
    if (!empty($search)) {
        $search = " AND ({$search}) ";
        if (!is_user_logged_in())
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }
    return $search;
}
add_filter('posts_search', '__search_by_title_only', 500, 2);

// Disable WP Core lazy-loading
//add_filter( 'wp_lazy_loading_enabled', '__return_false' );
