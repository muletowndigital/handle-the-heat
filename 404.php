<?php get_header(); ?>

	<div class="content">
		
		<div class="container">	

			<div class="main">
				<h2>404 - Page Not Found</h2>
			    <p>Sorry, this page could not be found. The page you are looking for may have been moved, or the address may be typed incorrectly.</p>
		   		<p>The following pages may be helpful:</p>
				<?php wp_nav_menu( array('menu' => 'primary', 'depth' => '2') ); ?>
			</div>			

			<div class="sidebar launch">
				<?php get_sidebar('launch'); ?>
			</div>

		</div>

	</div>

<?php get_footer(); ?>