

	<?php if( get_field('popup_form_id') ) { ?>

		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style-popup.css" />

		<a class="colourbox_pop" href="#inline_content">
			<img class="popup_form_link_image" src="<?php the_field('popup_form_link_image'); ?>">	
		</a>

		<div style="display:none;">
			<div id="inline_content">
				<div class="preview_grid content">
					<div class="main full">
						<?php $popupformid = get_field('popup_form_id'); ?>
						<?php echo do_shortcode('[gravityform id="' . $popupformid . '" title="false" description="false" ajax="true"]'); ?>
		    		</div>
				</div>
			</div>
		</div>

	<?php } ?>

