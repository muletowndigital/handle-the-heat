<?php 
    // Template Name: Start Here Page
?>

<?php get_header(); ?>

    <div class="content">
        
        <div class="container">
                    
            <div class="main full start-here">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php remove_filter( 'the_content', 'wpautop' ); the_content(); ?>

				<?php endwhile; endif; ?>
				
				<div class="starthere-about" id="about-tessa"> <!-- starthere-about -->
				  <div class="starthere-about-img"><img src="<?php echo get_field('sh_about_tessa_image'); ?>"/></div>
                    <div class="starthere-about-right">
                        <div class="starthere-about-rightcontent">
                            <div class="starthere-about-rightcontent-inner"><?php the_field('sh_about_tessa_content'); ?></div>
                        </div>
                    </div>
                </div> <!-- starthere-about -->
                
                <div class="sh-baking-challenge" id="monthly-baking-challenge">
                    <h4><?php the_field('sh_baking_challenge_heading'); ?> <br> <hr> </h4>
                    <div class="sh-baking-challenge-outer">
                    <div class="sh-baking-challenge-inner">
                    	<div class="sh-baking-challenge-img"><img src="<?php echo get_field('sh_baking_challenge_image'); ?>"/></div>
                        <div class="sh-baking-challenge-content"><?php the_field('sh_baking_challenge_content'); ?>
                        <script type="text/javascript">
        function handleChange() {
            var arr = document.getElementById( 'challenge' ) ;
            window.location.href = arr.value ;
            //window.location(arr.value, "_blank");
        } 
        </script>
                        <div class="jumpto"> Jump to a challenge &nbsp;
                            <?php    
                            $args = array('cat'=>'537,2093');
                            $query = new WP_Query( $args );
                            echo '<select name="challenge" id="challenge" onchange="handleChange()">';
                            echo '<option> Please Select</option>';
                            // The Loop
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    echo '<option value="'.get_the_permalink().'">' . get_the_title() . '</option>';
                                    
                                        
                                }
                            } 
                            echo '</select>';
                            /* Restore original Post Data */
                            wp_reset_postdata();
                            ?>
                        </div> <!-- jumpto -->
                        
                        </div>
                    </div>
                    </div>
                    
                </div> <!-- sh-baking-challenge -->
				
				<div class="explore-bar sh-baking-tips" id="baking-tips"> <!-- sh-baking-tips -->
				    <div class="cupcakeimg"><img class="aligncenter size-full wp-image-13451" src="http://www.handletheheat.com/wp-content/uploads/2017/04/cupcake.png" alt="" width="213" height="71"></div>
				    <h4><?php the_field('sh_baking_science_&_tips_heading'); ?> <br> <hr> </h4>
				<ul id="recipe_grid">
				
				<?php $posts = get_field('baking_science_&_tips');

						if( $posts ): ?>

						    <?php foreach( $posts as $post): ?>

						        <?php setup_postdata($post); ?>

								<li class="sh-baking-tips-content">

									<?php get_template_part('tpl-exploremore'); ?>

								</li>

						    <?php endforeach; ?>

						    <?php wp_reset_postdata(); ?>

						<?php endif; ?>	
				</ul>
				</div> <!-- explore-bar --> <!-- sh-baking-tips -->
				
				<div class="sh-all-things-cookies" id="all-things-cookies"> <!-- sh-all-things-cookies -->
				<h4><?php the_field('sh_all_things_cookies_heading'); ?> <br> <hr> </h4>
				<div class="sh-all-things-cookies-headerimg"><img src="<?php the_field('sh_all_things_cookies_header_image'); ?>"/></div>
				<div class="sh-all-things-cookies-content">
				    <?php the_field('sh_all_things_cookies_content'); ?>
				</div>
				<div class="sh-all-things-cookies-content2">   
				    
				   
				   
				   <div class="sh-fav-recipes">

<div class="subheading"><?php the_field('sh_fav_cookie_recipe_heading'); ?></div>

<div class="outercontainer">
  <div class="carousel">
      <div class="slideControls">
            <a class="slidePrev">
              <i class="fa fa-angle-left"></i>
             </a>
            <a class="slideNext">
              <i class="fa fa-angle-right"></i>
            </a>
      </div> 
      <ul id="content-slider">
<?php $posts = get_field('sh_fav_cookie_recipes'); // most popular recipes

					if( $posts ): ?>

					    <?php foreach( $posts as $post): ?>

					        <?php setup_postdata($post); ?>

							<li class="mix bestrecipe">

								<?php get_template_part('tpl-bestofbest'); ?>

							</li>

					    <?php endforeach; ?>

					    <?php wp_reset_postdata(); ?>

					<?php endif; ?>
</ul>
  </div>
</div>
</div>
</div>
<div class="sh-all-things-cookies-content3">
<div class="sh-digital-books">
				    <div class="heading"><?php the_field('sh_digital_books_heading'); ?></div>
				    <div>
    				    <div class="sh-digital-book1">
    				        <img src="<?php the_field('sh_digital_book_image1'); ?>"/>
    				        <?php the_field('sh_digital_book_content1'); ?>
    				    </div>
    				    <div class="sh-digital-book2">
    				        <img src="<?php the_field('sh_digital_book_image2'); ?>"/>
    				        <?php the_field('sh_digital_book_content2'); ?>
    				    </div>
				    </div>
				   </div> <!-- sh-digital-books -->

				</div> <!-- sh-all-things-cookies -->
				<div class="sh-atc-optin">
				<div class="optin">
				<div class="text">
                                        <div class="img-outer">
					<img src="<?php echo get_template_directory_uri(); ?>/images/cookie-guide.png" alt="Cookie Customization Guide">
                                        </div>
                                        <div class="text-outer">
					<h3>Free <span> Cookie Customization Guide</span></h3>
					<p>Learn how to create your OWN cookie recipe that everyone will beg you to share!</p>
                                        </div>
				</div>
				<?php the_field('sh_all_things_cookies_opt_in'); ?>
			</div>
				    </div>
				</div>
				
				
				
				
				
				<!-- explore -->
				<div class="explore-bar sh-explore-bar" id="best-recipes">
                    <div class="heading"><?php the_field('sh_best_recipes_heading'); ?><hr/></div>
                    <div class="subheading"><?php the_field('sh_best_recipes_sub_heading'); ?></div>


				<ul class="filters">
                    <?php if( get_field('sh_best_recipes1_heading') ) {  ?>
                    <li><a class="filter" data-filter=".bestrecipes1"><?php the_field('sh_best_recipes1_heading'); ?></a></li>
                    <?php } ?>
                    <?php if( get_field('sh_best_recipes2_heading') ) {  ?>
                    <li><a class="filter" data-filter=".bestrecipes2"><?php the_field('sh_best_recipes2_heading'); ?></a></li>
                    <?php } ?>
                    <?php if( get_field('sh_best_recipes3_heading') ) {  ?>
                    <li><a class="filter" data-filter=".bestrecipes3"><?php the_field('sh_best_recipes3_heading'); ?></a></li>
                    <?php } ?>
                </ul>
                
                <ul id="recipe_grid">
                    <?php if( get_field('sh_best_recipes1_heading') ) { // seasonal faves if they are specified ?> 
                    <?php $posts = get_field('sh_best_recipes1');
                    if( $posts ): ?>
                    <?php foreach( $posts as $post): ?>
                    <?php setup_postdata($post); ?>
                    <li class="mix bestrecipes1"><?php get_template_part('tpl-exploremore'); ?></li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>					
                    <?php } ?>    
                    
                    <?php if( get_field('sh_best_recipes2_heading') ) { // seasonal faves if they are specified ?> 
                    <?php $posts = get_field('sh_best_recipes2');
                    if( $posts ): ?>
                    <?php foreach( $posts as $post): ?>
                    <?php setup_postdata($post); ?>
                    <li class="mix bestrecipes2"><?php get_template_part('tpl-exploremore'); ?></li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>					
                    <?php } ?> 
                    
                    <?php if( get_field('sh_best_recipes3_heading') ) { // seasonal faves if they are specified ?> 
                    <?php $posts = get_field('sh_best_recipes3');
                    if( $posts ): ?>
                    <?php foreach( $posts as $post): ?>
                    <?php setup_postdata($post); ?>
                    <li class="mix bestrecipes3"><?php get_template_part('tpl-exploremore'); ?></li>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>					
                    <?php } ?> 
                    
                </ul>

				

			</div><!-- explore -->
				
			<div class="sh-mofb" id="magic-of-baking">
			    <h4><?php the_field('sh_mofb_heading'); ?> <br> <hr> </h4>
			    <div class="subheading"><?php the_field('sh_mofb_sub_heading'); ?></div>
			    <div class="sh-mofb-content"><?php the_field('sh_mofb_content'); ?></div>
			</div>
			
			<div class="optin">
				<div class="text">
                                        <div class="img-outer">
					<img src="<?php echo get_template_directory_uri(); ?>/images/baking-secrets-new.png" alt="10 Baking Secrets You Need to Know">
                                        </div>
                                        <div class="text-outer">
					<h3>Want to <span> Bake Like a Pro</span>?</h3>
					<p>Join 60,000 others and receive my FREE visual guide: 10 Baking Secrets You NEED to Know!</p>
                                        </div>
				</div>
				<?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]'); ?>
			</div>

            </div>

		</div>

	</div>


<?php get_footer(); ?>