<?php 
    // Template Name: Past Challenge Page
?>

<?php get_header(); ?>

    <div class="content">
        <div class="container page-challenge">
            <div class="full">
                <div class="welcome">Welcome!</div>
                <div class="mc-mainsection"> <!-- mcrmainsection-->
                    <div class="mc-mainsection-title"><h1 class="text"><?php the_title(); ?></h1></div>
                    <div class="mc-mainsection-inner">
                        <div class="mc-mainsection-content">
                            <div><?php echo get_field('mc_main_section_content'); ?></div>
                            <div class="mc-mainsection-content-buttons">
                                <?php if( have_rows('mc_main_section_content_buttons') ): 
                                while( have_rows('mc_main_section_content_buttons') ): the_row(); 
                                  // vars
                                $mc_main_section_content_button1_title = get_sub_field('mc_main_section_content_button1_title', false, false);
                                $mc_main_section_content_button2_title = get_sub_field('mc_main_section_content_button2_title');
                                $mc_main_section_content_button3_title = get_sub_field('mc_main_section_content_button3_title');
                                $mc_main_section_content_button1_link = get_sub_field('mc_main_section_content_button1_link');
                                $mc_main_section_content_button2_link = get_sub_field('mc_main_section_content_button2_link');
                                $mc_main_section_content_button3_link = get_sub_field('mc_main_section_content_button3_link');
                                  
                                
                                if(!empty($mc_main_section_content_button1_title)){
                                    echo '<div class="mc-mainsection-content-button">'.$mc_main_section_content_button1_title.'</div>';  
                                }
                                if(!empty($mc_main_section_content_button2_title)){
                                    echo '<div class="mc-mainsection-content-button"><a href="'.$mc_main_section_content_button2_link.'" target="_blank"/>'.$mc_main_section_content_button2_title.'</a></div>';  
                                }
                                if(!empty($mc_main_section_content_button3_title)){
                                    echo '<div class="mc-mainsection-content-button"><a href="'.$mc_main_section_content_button3_link.'" target="_blank"/>'.$mc_main_section_content_button3_title.'</a></div>';  
                                }
                                ?>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="mc-mainsection-img"><a href="<?php echo get_field('mc_main_section_image_link'); ?>"><img src="<?php echo get_field('mc_main_section_image'); ?>"/></a></div>
                        <div class="mc-mainsection-img-tagline"><?php echo get_field('mc_main_section_image_tagline'); ?></div>
                    </div>
                </div><!-- mcrmainsection-->
                
                <div class="mc-section-buttons">
                    <ul>
                    <?php if(get_field('mc_sponsor_content')) { 
                        echo "<li><a href='#sponsored-by'>Sponsored By</a></li>";
                    } ?>
                    <?php if(get_field('mc_how_it_works_title')) { 
                        echo "<li><a href='#how-it-works'>How It Works</a></li>";
                    } ?>
                    <?php if(get_field('mc_past_challenge_winners_title')) { 
                        echo "<li><a href='#past-winners'>Past Winners</a></li>";
                    } ?>
                    <?php if(get_field('mc_about_tessa_titlte')) { 
                        echo "<li><a href='#about-tessa'>About Tessa</a></li>";
                    } ?>
                    <?php if(get_field('mc_how_it_works_title')) { 
                        echo "<li><a href='#submit-entry'>Submit Your Entry</a></li>";
                    } ?>
                    </ul>
                </div>
                
                <?php if(get_field('mc_sponsor_title')) { ?>
                <div class="mc-sponsor" id="sponsored-by"> <!-- mcrsponsor-->
                <h4><?php the_field('mc_sponsor_title'); ?> <br><hr></h4>
                <div class="mc-sponsor-subtitle"><?php the_field('mc_sponsor_sub_title'); ?></div>
				<?php if(get_field('mc_sponsor_video')) { ?>
                	<div class="mc-sponsor-video"><div class="aspect-ratio"><?php echo get_field('mc_sponsor_video'); ?></div></div>
				<?php } ?>
                <div class="mc-sponsor-content"><?php the_field('mc_sponsor_content'); ?></div>
                <div class="mc-sponsor-content-buttons">
                    <?php if( have_rows('mc_sponsor_content_buttons') ): 
                          while( have_rows('mc_sponsor_content_buttons') ): the_row(); 
                          // vars
                          $mc_sponsor_content_button1 = get_sub_field('mc_sponsor_content_button1');
                          $mc_sponsor_content_button2 = get_sub_field('mc_sponsor_content_button2');
                          $mc_sponsor_content_button3 = get_sub_field('mc_sponsor_content_button3');
                          $mc_sponsor_content_button1_link = get_sub_field('mc_sponsor_content_button1_link');
                          $mc_sponsor_content_button2_link = get_sub_field('mc_sponsor_content_button2_link');
                          $mc_sponsor_content_button3_link = get_sub_field('mc_sponsor_content_button3_link');
                          
                          if(!empty($mc_sponsor_content_button1)){
                            echo '<div class="mc-sponsor-content-button">'.$mc_sponsor_content_button1.'</div>';  
                          }
                          if(!empty($mc_sponsor_content_button2)){
                            echo '<div class="mc-sponsor-content-button">'.$mc_sponsor_content_button2.'</div>';  
                          }
                          if(!empty($mc_sponsor_content_button3)){
                            echo '<div class="mc-sponsor-content-button">'.$mc_sponsor_content_button3.'</div>';  
                          }
                    ?>
                    
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                </div> <!-- mcrsponsor-->
                <?php } ?>
                
                <div class="mc-prizes"> <!-- mcprizes-->
                    <h4><?php the_field('mc_prizes_title'); ?><br><hr></h4>
                    <div class="mc-prizes-inner">
                        <?php if( have_rows('mc_prizes') ): 
                              while( have_rows('mc_prizes') ): the_row(); 
                              // vars
                              
                              $mc_prize1_image = get_sub_field('mc_prize1_image');
                              $mc_prize1_content = get_sub_field('mc_prize1_content');
                              $mc_prize2_image = get_sub_field('mc_prize2_image');
                              $mc_prize2_content = get_sub_field('mc_prize2_content');
                              $mc_prize3_image = get_sub_field('mc_prize3_image');
                              $mc_prize3_content = get_sub_field('mc_prize3_content');
                              
                              
                              if(!empty($mc_prize1_content)){
                                echo '<div class="mc-prize1">
                                        <div><img src="'.$mc_prize1_image.'"/></div>
                                        <div>'.$mc_prize1_content.'</div>
                                      </div>';  
                              }
                              if(!empty($mc_prize2_content)){
                                echo '<div class="mc-prize2">
                                        <div><img src="'.$mc_prize2_image.'"/></div>
                                        <div>'.$mc_prize2_content.'</div>
                                      </div>';  
                              }
                              if(!empty($mc_prize3_content)){
                                echo '<div class="mc-prize3">
                                        <div><img src="'.$mc_prize3_image.'"/></div>
                                        <div>'.$mc_prize3_content.'</div>
                                      </div>';  
                              }
                        ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div> <!-- mcprizes-->
                
                <div class="mc-howitworks" id="how-it-works"> <!-- mchowitworks-->
                    <h4><?php the_field('mc_how_it_works_title'); ?><br><hr></h4>
                    <div class="mc-howitworks-subtitle"><?php the_field('mc_how_it_works_sub_title'); ?></div>
                    <div class="mc-steps">
                        <?php if( have_rows('mc_how_it_works_steps') ): 
                              while( have_rows('mc_how_it_works_steps') ): the_row(); 
                              // vars
                              
                              $mc_how_it_works_step1_image = get_sub_field('mc_how_it_works_step1_image');
                              $mc_how_it_works_step1_content = get_sub_field('mc_how_it_works_step1_content');
                              $mc_how_it_works_step2_image = get_sub_field('mc_how_it_works_step2_image');
                              $mc_how_it_works_step2_content = get_sub_field('mc_how_it_works_step2_content');
                              $mc_how_it_works_step3_image = get_sub_field('mc_how_it_works_step3_image');
                              $mc_how_it_works_step3_content = get_sub_field('mc_how_it_works_step3_content');
                              
                              
                              if(!empty($mc_how_it_works_step1_content)){
                                echo '<div class="mc-step1">
                                        <div class="mc-step-img"><img src="'.$mc_how_it_works_step1_image.'"/></div>
                                        <div>'.$mc_how_it_works_step1_content.'</div>
                                      </div>';  
                              }
                              if(!empty($mc_how_it_works_step2_content)){
                                echo '<div class="mc-step2">
                                        <div class="mc-step-img"><img src="'.$mc_how_it_works_step2_image.'"/></div>
                                        <div>'.$mc_how_it_works_step2_content.'</div>
                                      </div>';  
                              }
                              if(!empty($mc_how_it_works_step3_content)){
                                echo '<div class="mc-step3">
                                        <div class="mc-step-img"><img src="'.$mc_how_it_works_step3_image.'"/></div>
                                        <div>'.$mc_how_it_works_step3_content.'</div>
                                      </div>';  
                              }
                        ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div> <!-- mcsteps-->
                    
                </div> <!-- mchowitworks-->
                
                <div class="mc-howitworksdetails" id="submit-entry"> <!-- mchowitworksdetails-->
                    <div><?php the_field('mc_how_it_works_details'); ?></div>
                </div><!-- mchowitworksdetails-->
                
                <div class="mc-pcw" id="past-winners"> <!-- pastchallengewinners -->
                    <h4><?php the_field('mc_past_challenge_winners_title'); ?><br><hr></h4>
                    <div class="mc-pcw-inner">
                        <?php if( have_rows('mc_past_challenge_winners') ): 
                              while( have_rows('mc_past_challenge_winners') ): the_row(); 
                              // vars
                              
                              $mc_past_challenge_winner1_image = get_sub_field('mc_past_challenge_winner1_image');
                              $mc_past_challenge_winner1_monthyear = get_sub_field('mc_past_challenge_winner1_monthyear');
                              $mc_past_challenge_winner1_nameplace = get_sub_field('mc_past_challenge_winner1_nameplace');
                              $mc_past_challenge_winner1_challengename = get_sub_field('mc_past_challenge_winner1_challengename');
                              $mc_past_challenge_winner1_challengelink = get_sub_field('mc_past_challenge_winner1_challengelink');
                              
                              $mc_past_challenge_winner2_image = get_sub_field('mc_past_challenge_winner2_image');
                              $mc_past_challenge_winner2_monthyear = get_sub_field('mc_past_challenge_winner2_monthyear');
                              $mc_past_challenge_winner2_nameplace = get_sub_field('mc_past_challenge_winner2_nameplace');
                              $mc_past_challenge_winner2_challengename = get_sub_field('mc_past_challenge_winner2_challengename');
                              $mc_past_challenge_winner2_challengelink = get_sub_field('mc_past_challenge_winner2_challengelink');
                              
                              $mc_past_challenge_winner3_image = get_sub_field('mc_past_challenge_winner3_image');
                              $mc_past_challenge_winner3_monthyear = get_sub_field('mc_past_challenge_winner3_monthyear');
                              $mc_past_challenge_winner3_nameplace = get_sub_field('mc_past_challenge_winner3_nameplace');
                              $mc_past_challenge_winner3_challengename = get_sub_field('mc_past_challenge_winner3_challengename');
                              $mc_past_challenge_winner3_challengelink = get_sub_field('mc_past_challenge_winner3_challengelink');
                              
                              $mc_past_challenge_winner4_image = get_sub_field('mc_past_challenge_winner4_image');
                              $mc_past_challenge_winner4_monthyear = get_sub_field('mc_past_challenge_winner4_monthyear');
                              $mc_past_challenge_winner4_nameplace = get_sub_field('mc_past_challenge_winner4_nameplace');
                              $mc_past_challenge_winner4_challengename = get_sub_field('mc_past_challenge_winner4_challengename');
                              $mc_past_challenge_winner4_challengelink = get_sub_field('mc_past_challenge_winner4_challengelink');
                              
                              if(!empty($mc_past_challenge_winner1_image)){
                                echo '<div class="mc-pcw-winner1">
                                        <div class="mc-pcw-img"><a href="'.$mc_past_challenge_winner1_challengelink.'" target="_blank"/><img src="'.$mc_past_challenge_winner1_image.'"/></a></div>
                                        <div class="mc-pcw-date">'.$mc_past_challenge_winner1_monthyear.'</div>
                                        <div class="mc-pcw-nameplace">'. $mc_past_challenge_winner1_nameplace.'</div>
                                        <div class="mc-pcw-challengename"><a href="'.$mc_past_challenge_winner1_challengelink.'"/>'.$mc_past_challenge_winner1_challengename.'</a></div>
                                      </div>';  
                              }
                              if(!empty($mc_past_challenge_winner2_image)){
                                echo '<div class="mc-pcw-winner2">
                                        <div><a href="'.$mc_past_challenge_winner2_challengelink.'" target="_blank"/><img src="'.$mc_past_challenge_winner2_image.'"/></a></div>
                                        <div class="mc-pcw-date">'.$mc_past_challenge_winner2_monthyear.'</div>
                                        <div>'. $mc_past_challenge_winner2_nameplace.'</div>
                                        <div class="mc-pcw-challengename"><a href="'.$mc_past_challenge_winner2_challengelink.'"/>'.$mc_past_challenge_winner2_challengename.'</a></div>
                                      </div>';  
                              }
                              if(!empty($mc_past_challenge_winner3_image)){
                                echo '<div class="mc-pcw-winner3">
                                        <div><a href="'.$mc_past_challenge_winner3_challengelink.'" target="_blank"/><img src="'.$mc_past_challenge_winner3_image.'"/></a></div>
                                        <div class="mc-pcw-date">'.$mc_past_challenge_winner3_monthyear.'</div>
                                        <div>'. $mc_past_challenge_winner3_nameplace.'</div>
                                        <div class="mc-pcw-challengename"><a href="'.$mc_past_challenge_winner3_challengelink.'"/>'.$mc_past_challenge_winner3_challengename.'</a></div>
                                      </div>';  
                              }
                              if(!empty($mc_past_challenge_winner4_image)){
                                echo '<div class="mc-pcw-winner4">
                                        <div><a href="'.$mc_past_challenge_winner4_challengelink.'" target="_blank"/><img src="'.$mc_past_challenge_winner4_image.'"/></a></div>
                                        <div class="mc-pcw-date">'.$mc_past_challenge_winner4_monthyear.'</div>
                                        <div>'. $mc_past_challenge_winner4_nameplace.'</div>
                                        <div class="mc-pcw-challengename"><a href="'.$mc_past_challenge_winner4_challengelink.'"/>'.$mc_past_challenge_winner4_challengename.'</a></div>
                                      </div>';  
                              }
                        ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    
                </div> <!-- pastchallengewinners -->
                
                <div class="mc-nextoptin"> <!-- mcnextoptin-->
                    <div class="mc-nextoptin-inner">
        				<div class="mc-nextoptin-text">
                            <div class="nextoptin-text"><?php the_field('mc_signup_next_challenge_text'); ?></div>
                        </div>
        				<div class="mc-nextoptin-button">
        				        <div class="nextoptin-button"><?php the_field('mc_signup_next_challenge_button_text'); ?></div>
        				</div>
        			</div>   
                </div> <!-- mcnextoptin-->
                <div class="mc-aboutrecipe"> <!-- mcaboutrecipe -->
                    <div class="mc-aboutrecipetitle"><div class="text"><?php the_field('mc_about_recipe_title'); ?></div></div>
                    <div class="mc-aboutrecipe-outer">
                    <div class="mc-aboutrecipe-inner">
                        <div class="mc-aboutrecipe-gallery"><?php echo do_shortcode(get_field('mc_about_recipe_gallery')); ?></div>
                        <div class="mc-aboutrecipe-content"><?php the_field('mc_about_recipe_content'); ?></div>
                    </div>
                    </div>
                </div> <!-- mcaboutrecipe -->
                <div class="mc-favtools">
                    <h4><?php the_field('mc_fav_tools_title'); ?><br><hr></h4>
                    <div class="mc-favtools-subtitle"><?php the_field('mc_fav_tools_sub_title'); ?></div>
                    <div class="mc-favtools-widget"><?php the_field('mc_fav_tools_widget_code'); ?></div>
                </div>
                <div class="mc-bakingchallenge" id="challenge-info"> <!-- mc-bakingchallenge-->
                    <div class="mc-bakingchallenge-section">
                        <div class="mc-bakingchallenge-section-title"><?php the_field('mc_baking_challenge_section_title'); ?></div>
                        <div class="mc-bakingchallenge-section-inner">
                        <div class="mc-bakingchallenge-section-img"><img src="<?php echo get_field('mc_baking_challenge_section_image'); ?>"/></div>
                        <div class="mc-bakingchallenge-section-content">
                            <div><?php the_field('mc_baking_challenge_section_content'); ?></div>
                            <script type="text/javascript">
                            function handleChange() {
                                var arr = document.getElementById( 'challenge' ) ;
                                window.location.href = arr.value ;
                                //window.location(arr.value, "_blank");
                            } 
                            </script>
                            <div class="mc-pastchallenge"><!-- mc-pastchallenge -->
                            <div><strong>View our past challenges:</strong></div>
                            <div class="jumpto"> Jump to a challenge &nbsp;
                                    <?php    
                                    $args = array('cat'=>'537,2093');
                                    $query = new WP_Query( $args );
                                    echo '<select name="challenge" id="challenge" onchange="handleChange()">';
                                    echo '<option> Please Select</option>';
                                    // The Loop
                                    if ( $query->have_posts() ) {
                                        while ( $query->have_posts() ) {
                                            $query->the_post();
                                            echo '<option value="'.get_the_permalink().'">' . get_the_title() . '</option>';
                                            
                                                
                                        }
                                    } 
                                    echo '</select>';
                                    /* Restore original Post Data */
                                    wp_reset_postdata();
                                    ?>
                            </div> <!-- jumpto -->
                            </div> <!-- mc-pastchallenge -->
                            </div> <!-- mc-bakingchallenge-section-content -->
                            </div>
                           
                        
                    </div>
                    <div class="mc-bakingchallenge-feed"><?php the_field('mc_social_feed_gallery'); ?></div>
                </div><!-- mc-bakingchallenge-->
        
                <div class="mc-optin"> <!--mcoptin -->
                    <div class="mc-optin-inner">
						<?php
							 $class = "";
							if ( !get_field('mc_opt_in_image') ) {
								$class = " no-optin-img";
							}
						?>
        				<div class="mc-optin-text<?php echo $class; ?>">
                                <div class="optin-img"><img src="<?php the_field('mc_opt_in_image'); ?>"></div>
                                <div class="optin-text<?php echo $class; ?>"><?php the_field('mc_opt_in_text'); ?></div>
                                
        				</div>
        				<div class="mc-optin-button<?php echo $class; ?>">
        				        <div class="optin-button"><?php the_field('mc_opt_in_button_text'); ?></div>
        				</div>
        			</div>
    		    </div><!--mcoptin -->
                <div class="mc-about" id="about-tessa"> <!-- mc-about -->
                    <h4><?php the_field('mc_about_tessa_titlte'); ?><br><hr></h4>
        		    <div class="mc-about-img"><img src="<?php echo get_field('mc_about_tessa_image'); ?>"/></div>
                    <div class="mc-about-right">
                        <div class="mc-about-rightcontent">
                            <div class="mc-about-rightcontent-inner"><?php the_field('mc_about_tessa_content'); ?></div>
                        </div>
                    </div>
                </div> <!-- mc-about -->
                
            </div>
        </div>
    </div>
    
<?php get_footer(); ?>