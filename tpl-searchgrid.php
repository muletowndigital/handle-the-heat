<div class="recipe-filtering">

    <ul id="search-recipe-grid">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <li class="mix">
                <?php get_template_part('tpl-filter-recipe-brick'); ?>
            </li>

        <?php endwhile; endif; ?>



    </ul>
</div>

<div id="recipes-pagination" class="pagination">
    <?php global $wp_query;
    $big = 999999999; // need an unlikely integer
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
    ?>
</div>
